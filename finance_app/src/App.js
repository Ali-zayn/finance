// import {useState, useEffect} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import Categories from "./components/Categories/Categories";
import Admins from "./components/Admins_Display/Admins";
import Goals from "./components/Profit_Goals/Goals";
import Login_info from './components/Login_Info/Login_info'; 
import Currency from './components/Currency/Currency';
import Transactions from './components/Transactions/Transactions';

const App = () => {
    return (
        <>
            <Router>
                <Routes>
                    <Route path='/' exact element={<Login/>}/>
                    <Route path='/login' exact element={<Login/>}/>
                    <Route path='/categories' exact element={<Categories/>}/>
                    <Route path='/home' exact element={<Home/>}/>
                    <Route path='/admins' exact element={<Admins/>}/>
                    <Route path='/goals' exact element={<Goals/>}/>
                    <Route path='/login_info' exact element={<Login_info/>}/>
                    <Route path='/currency' exact element={<Currency/>}/>
                    <Route path='/transactions' exact element={<Transactions/>}/>
                </Routes>
            </Router>
        </>
    );
};

export default App;
