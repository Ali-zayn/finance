import React, {useState, useEffect} from 'react';
import './Currency.css';
import Sidebar from '../Sidebar/Sidebar';
import { BiEditAlt } from 'react-icons/bi';
import { MdDelete } from 'react-icons/md';
import { GiCreditsCurrency } from "react-icons/gi";
import Popup from '../Popup/Popup';
import {FiPlusCircle} from "react-icons/fi";
import LoadingEffect from "../LoadingEffect/LoadingEffect";
import TransPagination from "../Transactions/TransPagination";
import {HiPlus} from "react-icons/hi";

function Currency() {
  const token = localStorage.getItem("token");
  if (!token) {
    window.location.href = "/login";
  }
  const Role = localStorage.getItem("role");
  const [popup, setPopup] = useState(false);
  const [popupErr, setPopupErr] = useState(false);
  const [popupCur, setPopupCur] = useState(false);
  const [popupAdd, setPopupAdd]=useState(false);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const[newName,setNewName] = useState("");
  const[newRate,setNewRate] = useState("");
  const[newISO,setNewISO] = useState("");
  const[newSymbol,setNewSymbol] = useState("");
  const[newNameAdd,setNewNameAdd] = useState("");
  const[newRateAdd,setNewRateAdd] = useState("");
  const[newISOAdd,setNewISOAdd] = useState("");
  const[newSymbolAdd,setNewSymbolAdd] = useState("");
    const [currencyID, setCurrencyID] = useState(null);

    // pagination for Currencies
    const [currentCurrencyPage, setCurrentCurrencyPage] = useState(1);
    const [CurrenciesPerPage, setCurrenciesPerPage] = useState(5);
    const indexOfLastCurrency = currentCurrencyPage * CurrenciesPerPage;
    const indexOfFirstCurrency = indexOfLastCurrency - CurrenciesPerPage;

    useEffect(() => {
        FetchData();

    }, []);


    // Get All Currencies ///////////////////////////////////////////////////////////////////////////////
    const FetchData = () => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/currency`, {
            method: "GET",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                setData(data);
                setNewNameAdd("");
                setNewISOAdd('');
                setNewRateAdd('');
                setNewSymbolAdd('');
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    };
    // Add Currency //////////////////////////////////////////////////////////////////////////////
    const AddCurrency = () => {
            setLoading(true);
            fetch(`${process.env.REACT_APP_BACKEND_URL}api/currency?name=${newNameAdd}&slug=${newNameAdd}&ISO=${newISOAdd}&Symbol=${newSymbolAdd}&rate=${newRateAdd}`, {
                method: "POST",
                headers: {"Content-Type": "application/json"},
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
                .then((data) => {
                    setLoading(false);
                    FetchData();
                })
                .catch((error) => {
                    console.error(error.message);
                    setError(error);
                });

    }
    // Edit Currency //////////////////////////////////////////////////////////////////////////////
    const EditCurrency = () => {

            setLoading(true);
            fetch(`${process.env.REACT_APP_BACKEND_URL}api/currency/${currencyID}?name=${newName}&slug=${newName}&ISO=${newISO}&Symbol=${newSymbol}&rate=${newRate}`, {
                method: "PUT",
                headers: {"Content-Type": "application/json"},
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
                .then((data) => {
                    setLoading(false);
                    FetchData();
                })
                .catch((error) => {
                    console.error(error.message);
                    setError(error);
                    setPopupErr(true); 
                });

    }
    // DELETE Currency //////////////////////////////////////////////////////////////////////////////
    const DeleteCurrency = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/currency/${currencyID}`, {
            method: "DELETE",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }

    if (loading) return (
            <LoadingEffect/>
    );
    if (error) {
        setPopupErr(true);
        setError(null);
};

  return (
    <div className='container'>
      <Sidebar />
      <div className='currency_wrapper'>
        <h1>Currency</h1>
              <button className='plus-butn point' onClick={()=>{setPopupAdd(true)}}>
              <HiPlus size='1.7rem' className='point add_admin' />
              </button>

          <table className='styled-table'>
          <thead>
            <tr>
              <th>Name</th>
              <th>ISO</th>
              <th>Symbol</th>
              <th>Rate</th>
              <th>Edits</th>
            </tr>
          </thead>
          <tbody>
          {data.slice(indexOfFirstCurrency, indexOfLastCurrency).map((item, index) => {
              return (

              <tr key={index}>
                  <td data-label="Name">{item.name}</td>

                  <td data-label="ISO">{item.ISO}</td>
                  <td data-label="Symbol">{item.Symbol}</td>
                  <td className="amount" data-label="Rate">{item.rate}</td>

                  <td data-label="Edits">
                      <div className='admin_edit_btns'>
                          {item.ISO !== "USD" ? 
                          <BiEditAlt size='1.5rem' className='point'
                          onClick={() => {
                              setCurrencyID(item.id);
                              setNewName(item.name);
                              setNewISO(item.ISO);
                              setNewSymbol(item.Symbol);
                              setNewRate(item.rate);
                              setPopup(true)

                      }}/> : ""}
                      </div>
                  </td>
              </tr>
              );
          })}
          </tbody>
        </table>
          <TransPagination
              totalPosts={data.length}
              postsPerPage={CurrenciesPerPage}
              setCurrentPage={setCurrentCurrencyPage}
              currentPage={currentCurrencyPage}/>
      </div>
        {/*///// Edit Currency Popup ///////////////////////////////////////////////////////////////*/}
      <Popup trigger={popup} setTrigger={setPopup}>
      <div className="popup_title">
                    <GiCreditsCurrency size="1.5rem" />
                    <h3>Edit Currency</h3>
                </div>
                <form onSubmit={()=>{EditCurrency(); setPopup(false);}}>
                    <div className="form-group">
                    <div className='input_label'>
                <label for="name">Name:</label>
                        <input
                            type="text"
                            name="name"
                            id="name"
                            placeholder="name"
                            value={newName}
                            className='admin_input'
                            onChange={(e) => {
                                setNewName(e.target.value)
                            }}

                        /> </div>
                        <div className='input_label'>
                <label for="ISO">ISO:</label>
                        <input
                            type="text"
                            name="ISO"
                            id="ISO"
                            placeholder="ISO"
                            className='admin_input'
                            value={newISO}
                            onChange={(e) => {
                                setNewISO(e.target.value)
                            }}
                            
                        /></div>
                        <div className='input_label'>
                <label for="Symbole">Symbol:</label>
                        <input
                            type="text"
                            name="Symbole"
                            id="Symbole"
                            placeholder="Symbole"
                            className='admin_input'
                            value={newSymbol}
                            onChange={(e) => {
                                setNewSymbol(e.target.value)
                            }}
                            
                        /></div>
                        <div className='input_label'>
                <label for="rate">Rate:</label>
                        <input
                            type="text"
                            name="rate"
                            id="rate"
                            placeholder="Rate"
                            className='admin_input'
                            value={newRate}
                            onChange={(e) => {
                                setNewRate(e.target.value)
                            }}
                            
                        /></div>
                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />
                        </div>
                        </form>
      </Popup>
        {/*///// Delete Confirmation Popup ///////////////////////////////////////////////////////////////*/}
      <Popup trigger={popupCur} setTrigger={setPopupCur}>
      <h3>Are you sure you want to delete this currency?</h3><br />
                <button className='confirmation' onClick={()=>{DeleteCurrency(); setPopupCur(false);}}>Yes</button>

      </Popup>
       {/* // Error handling Popup /////////////////////////////////////////////////// */}
       <Popup trigger={popupErr} setTrigger={setPopupErr}>
            <h3>You cannot enter a currency that already exists.</h3><br/>
            <button className='confirmation' onClick={()=>{setPopupErr(false); setError(null); setPopupAdd(false); setPopup(false)}}>Ok</button>

        </Popup>
        {/*///// ADD Currency Popup ///////////////////////////////////////////////////////////////*/}
        <Popup trigger={popupAdd} setTrigger={setPopupAdd}>
            <div className="popup_title">
                <GiCreditsCurrency size="1.5rem" />
                <h3>ADD NEW Currency</h3>
            </div>
            <form onSubmit={()=>{AddCurrency(); setPopupAdd(false);}}>
                <div className="form-group">
                <div className='input_label'>
                <label for="name">Name:</label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="name"
                        className='admin_input'
                        value={newNameAdd}
                        onChange={(e) => {
                            setNewNameAdd(e.target.value)
                        }}
                        required

                    /></div>
                    <div className='input_label'>
                <label for="ISO">ISO:</label>
                    <input
                        type="text"
                        name="ISO"
                        id="ISO"
                        placeholder="ISO"
                        className='admin_input'
                        value={newISOAdd}
                        onChange={(e) => {
                            setNewISOAdd(e.target.value)
                        }}
                        required
                    /></div>
                    <div className='input_label'>
                <label for="Symbol">Symbol:</label>
                    <input
                        type="text"
                        name="Symbol"
                        id="Symbol"
                        placeholder="Symbol"
                        className='admin_input'
                        value={newSymbolAdd}
                        onChange={(e) => {
                            setNewSymbolAdd(e.target.value)
                        }}
                        required
                    /></div>
                    <div className='input_label'>
                <label for="rate">Rate:</label>
                    <input
                        type="text"
                        name="rate"
                        id="rate"
                        placeholder="Rate"
                        className='admin_input'
                        value={newRateAdd}
                        onChange={(e) => {
                            setNewRateAdd(e.target.value)
                        }}
                        required

                    /> </div>
                    <input
                        type="submit"
                        className="confirm_btn"
                        value="Confirm"
                    />
                </div>
            </form>
      </Popup>
    </div>
          )
}

export default Currency;