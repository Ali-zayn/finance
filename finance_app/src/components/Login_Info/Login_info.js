import React, { useEffect, useState } from 'react';
import './Login_info.css';
import Sidebar from '../Sidebar/Sidebar';
import Popup from '../Popup/Popup';
import LoadingEffect from "../LoadingEffect/LoadingEffect";
import {FaUserEdit, FaEye,FaEyeSlash} from 'react-icons/fa';
import axios from "axios";
function Login_info() {
  const token = localStorage.getItem("token");
  if (!token) {
    window.location.href = "/login";
  }
  const id = localStorage.getItem('id');
  const onChangeFile = e => {
    setPicture(e.target.files[0]);
  }

  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [editPopup, setEditPopup] = useState(false);
  const [adminID, setAdminID] = useState(null);
  const[f_name, setF_name] = useState(null);
  const[l_name, setl_name] = useState(null);
  const[email, setEmail] = useState(null);
  const [phoneNumber, setPhoneNumber]=useState(null);
  const [showPassword, setShowPassword] = useState(false);
  const [picture, setPicture] = useState([]);
  const [conf, setConf] = useState(null);

  useEffect(() => {
    FetchData();

  }, []);

  // Get Admin ///////////////////////////////////////////////////////////////////////////////
  const FetchData = () => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}api/admins/${id}`, {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        console.log(data)
        setData(data);
        setAdminID(data[0][0].id)
        console.log(adminID);
        setUsername(data[0][0].username);
        setPassword(data[0][0].password);
        setConf(data[0][0].password);
        setF_name(data[0][0].f_name);
        setl_name(data[0][0].l_name);
        setEmail(data[0][0].email);
        setPhoneNumber(data[0][0].phoneNumber);
        setPicture(data[0][0].picture);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error.message);
        setError(error);
      });
  };

  // Edit Admin //////////////////////////////////////////////////////////////////////////////
  const EditAdmin = async() => {
    if (password === conf) {
      console.log(typeof(picture));
      setLoading(true);
      const formData = new FormData();
      formData.append("username", username);
      formData.append("f_name", f_name);
      formData.append("l_name", l_name);
      formData.append("password", password);
      formData.append("email", email);
      formData.append("phoneNumber", phoneNumber);
      if(typeof(picture) === "array" || typeof(picture) === "object") {
        formData.append("picture", picture);
      }
      formData.append("slug", username);
      formData.append("_method", "PUT");
      await axios.post(`${process.env.REACT_APP_BACKEND_URL}api/admins/${adminID}`, formData,{
        headers: {"Content-Type": "multipart/form-data"},
      })
          .then((res) => {
            console.log("updated");
            setLoading(false);
            FetchData();
          })

          .catch((error) => {
            console.log(error);
          });
    } else {
      return alert('Please confirm the password correctly');
    }
  }

  if (loading) return (

      <div className='container'>
        <Sidebar/>
        <LoadingEffect/>

      </div>
  );
  if (error) return "Error";
console.log(data)
  return (
    <div className='container'>
      <Sidebar />

      <div className='login_info_wrapper'>
        <h1>Profile Info</h1>

          <div className='first-part'>
            <div className='img_prf'>
            <img className='prf-pic' src={`${process.env.REACT_APP_BACKEND_URL}pictures/${data[0][0].picture}`} alt='Avatar'/>
          <FaUserEdit  size='2rem' className='edit-prf-btn' onClick={() => { setEditPopup(true) }} />
            </div>
            <div className='admin_info'>
          <p><b>First-name: </b> {f_name}</p>
              <p><b>Last-name: </b>{l_name}</p>
          <p><b>Email: </b> {email}</p>
              <p><b>Phone Number: </b>{phoneNumber}</p>
          </div>
          </div>
        <div className='second-part'>
        <p><b>Username: </b>{username}</p>
          <div className='show_password'><b>Password: </b>
          <input
              type={showPassword ? "text" : "password"}
              value={password}
              onChange={(e) => e.target.value}
              disabled
          />{" "}
          <button onClick={()=>{setShowPassword(!showPassword)}} className="btn reveal">
            {showPassword ? <FaEye size='1.5rem'/> : <FaEyeSlash size='1.5rem'/>}
          </button>
          </div>
        </div>
      </div>
      {/* // Edit Popup /////////////////////////////////////////////////////////////////////////// */}
       <Popup  trigger={editPopup} setTrigger={setEditPopup}>
         <div className="popup_title">
           <FaUserEdit size="1.5rem"/>
           <h3>Edit Admin</h3>
         </div>
         <form onSubmit={() => {
           EditAdmin();
           setEditPopup(false)
         }}>
           <div className="form-group">
             <div className='form_admin'>
               <div className='popup_left'>
                 <b><label htmlFor="f_name">First name</label></b>
                 <input
                     type="text"
                     name="f_name"
                     id="f_name"
                     placeholder="First-name"
                     className='admin_input'
                     value={f_name}
                     onChange={(e) => {
                       setF_name(e.target.value);
                     }}
                 />
                 <b><label htmlFor="l_name">Last name</label></b>
                 <input
                     type="text"
                     name="l_name"
                     id="l_name"
                     placeholder="Last-name"
                     className='admin_input'
                     value={l_name}
                     onChange={(e) => {
                       setl_name(e.target.value);
                     }}
                 />
                 <b><label htmlFor="email">Email</label></b>
                 <input
                     type="text"
                     name="email"
                     id="email"
                     placeholder="enter email"
                     className='admin_input'
                     value={email}
                     onChange={(e) => {
                       setEmail(e.target.value);
                     }}
                 />
                 <b><label htmlFor="phone">Phone number</label></b>
                 <input
                     type="text"
                     name="phone"
                     id="phone"
                     placeholder="enter phone-number"
                     className='admin_input'
                     value={phoneNumber}
                     onChange={(e) => {
                       setPhoneNumber(e.target.value);
                     }}
                 />
               </div>
               <div className='popup_right'>
                 <b><label htmlFor="name">Username</label></b>
                 <input
                     type="text"
                     name="name"
                     id="name"
                     placeholder="Username"
                     className='admin_input'
                     value={username}
                     onChange={(e) => {
                       setUsername(e.target.value)
                     }}
                 />
                     <div className='reset_pass'>
                       <b><label htmlFor="password">Password</label></b>

                       <input
                           type="text"
                           name="password"
                           id="password"
                           placeholder="Password"
                           className='admin_input'
                           value={password}
                           onChange={(e) => {
                             setPassword(e.target.value)
                           }}
                       />
                       <b><label htmlFor="conf_password">Confirm Password</label></b>
                       <input
                           type="text"
                           name="conf_password"
                           id="conf_password"
                           placeholder="Confirm Password"
                           className='admin_input'
                           value={conf}
                           onChange={(e) => {
                             setConf(e.target.value)
                           }}
                       /></div>
               </div>
             </div>
             <input
                 type="file"
                 name="picture"
                 onChange={(onChangeFile)}

             />
             <input
                 type="submit"
                 className="confirm_btn"
                 value="Confirm"
             />
           </div>
         </form>

       </Popup>
    </div>
  )
}

export default Login_info;
