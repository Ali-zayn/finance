import React, { useState, useEffect } from 'react';
import { Chart, LinearScale, CategoryScale, BarElement, Title, Legend, Tooltip } from 'chart.js';
import './Home.css';
import { Bar } from 'react-chartjs-2';
import {MdArrowForwardIos, MdArrowBackIosNew} from 'react-icons/md'
Chart.register(LinearScale, CategoryScale, BarElement, Title, Tooltip, Legend);

function BarChart(props) {
    const [years, setYears] = useState([2017, 2018, 2019, 2020, 2021, 2022]);
    const [incomes, setIncomes] = useState([]);
    const [expenses, setExpenses] = useState([]);
    const [incomesMonth, setIncomesMonth] = useState([]);
    const [expensesMonth, setExpensesMonth] = useState([]);
    const [incomesWeek, setIncomesWeek] = useState([]);
    const [expensesWeek, setExpensesWeek] = useState([]);
    const [iteration, setIteration] = useState(true);
    const [selectType, setSelectType] = useState("year");
    const months =["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const monthsIndex =[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    const weeks = ["Week_1", "Week_2", "Week_3", "Week_4"];
    const [yearMonths, setYearMonths] = useState(2022);
    const [yearMonthsweek, setYearMonthsweek] = useState(0);

    let data = {
        labels: years,
        datasets: [{
            label: "Incomes",
            data: incomes,
            backgroundColor: ["rgb(50, 50, 255)"],

        },
        {
            label: "Expenses",
            data: expenses,
            backgroundColor: ["rgb(255, 0, 0)"],

        }
        ]
    };

    let dataMonths = {
        labels: months,
        datasets: [{
            label: "Incomes",
            data: incomesMonth,
            backgroundColor: ["rgb(50, 50, 255)"],

        },
            {
                label: "Expenses",
                data: expensesMonth,
                backgroundColor: ["rgb(255, 0, 0)"],

            }
        ]
    };

    let dataweeks = {
        labels: weeks,
        datasets: [{
            label: "Incomes",
            data: incomesWeek,
            backgroundColor: ["rgb(50, 50, 255)"],

        },
            {
                label: "Expenses",
                data: expensesWeek,
                backgroundColor: ["rgb(255, 0, 0)"],

            }
        ]
    };

    let options = {
        legend: {
            display: true,
            position: 'top',
            labels: {
                fontColor: "#000080",
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        maintainAspectRatio: false
    }; 

    ///////// Filter Transactions by Year ///////////////////////////////////////////////////////////////////////
    const FilterData = () => {
        console.log("bruh");
        let expense = [];
        let income = [];
        years.forEach(element => {
            let amounti = 0;
            let amounte = 0;
            props.transData.transactions.filter(item => {
                if (item.date.includes(element)) {
                    if (item.subcategory.category.type.includes("expense")) {
                        amounte += item.amount / item.currency.rate;

                    } else {
                        amounti += item.amount / item.currency.rate;

                    }
                } 
            })

                expense.push(amounte.toFixed(0));
                income.push(amounti.toFixed(0));

        } 
        );
        setExpenses(expense);
        setIncomes(income);

    }
///////// Filter Transactions by Weeks ////////////////////////////////////////////////////////////////////////////
    const FilterDataForWeeks = () => {
        console.log("bruh Month");
        let expense = [];
        let income = [];
        let amountWeek1i = 0;
        let amountWeek2i= 0;
        let amountWeek3i = 0;
        let amountWeek4i = 0;
        let amountWeek1e = 0;
        let amountWeek2e = 0;
        let amountWeek3e = 0;
        let amountWeek4e = 0;
                props.transData.transactions.filter(item => {
                    if (item.date.includes(yearMonths)) {
                        if (parseInt(item.date.split('-')[1]) === yearMonthsweek + 1) {
                            if(parseInt(item.date.split('-')[2]) >= 1 && parseInt(item.date.split('-')[2]) <= 7 ) {
                                if (item.subcategory.category.type.includes("income")) {
                                    amountWeek1i = item.amount / item.currency.rate;
                                } else {
                                    amountWeek1e = item.amount / item.currency.rate;
                                }
                            } else if(parseInt(item.date.split('-')[2]) >= 8 && parseInt(item.date.split('-')[2]) <= 14 ) {
                                if (item.subcategory.category.type.includes("income")) {
                                    amountWeek2i = item.amount / item.currency.rate;
                                } else {
                                    amountWeek2e = item.amount / item.currency.rate;
                                }
                            } else if(parseInt(item.date.split('-')[2]) >= 15 && parseInt(item.date.split('-')[2]) <= 21 ) {
                                if (item.subcategory.category.type.includes("income")) {
                                    amountWeek3i = item.amount / item.currency.rate;
                                } else {
                                    amountWeek3e = item.amount / item.currency.rate;
                                }
                            } else {
                                if (item.subcategory.category.type.includes("income")) {
                                    amountWeek4i = item.amount / item.currency.rate;
                                } else {
                                    amountWeek4e = item.amount / item.currency.rate;
                                }
                            }
                        }
                    }
                })
        income.push(amountWeek1i);
        income.push(amountWeek2i);
        income.push(amountWeek3i);
        income.push(amountWeek4i);
        expense.push(amountWeek1e);
        expense.push(amountWeek2e);
        expense.push(amountWeek3e);
        expense.push(amountWeek4e);

                // if(amount === 0) {
                //     expense.push(0);
                //     income.push(0);
                // }
        setExpensesWeek(expense);
        setIncomesWeek(income);

    }

    /////// Filter Transactions by Months /////////////////////////////////////////////////////////////////////////////
    const FilterDataForMonths = () => {
        console.log("bruh Week");
        let expense = [];
        let income = [];
        monthsIndex.forEach(element => {
                let amounti = 0;
            let amounte = 0;
                props.transData.transactions.filter(item => {
                    if (item.date.includes(yearMonths)) {
                        if (parseInt(item.date.split('-')[1]) === element) {
                            if (item.subcategory.category.type.includes("expense")) {
                                amounte = item.amount / item.currency.rate;
                            } else {
                                amounti = item.amount / item.currency.rate;

                            }
                        }
                    }
                })
            expense.push(amounte.toFixed(0));
            income.push(amounti.toFixed(0));

            }
        );
        setExpensesMonth(expense);
        setIncomesMonth(income);

    }


    useEffect(() => {
        FilterData();
        FilterDataForMonths();
        FilterDataForWeeks();
    }, [iteration, ])

    const nextYear = () => {
        let arr = years;
        let i = arr.length - 1;
        let newYear = arr[i] + 1;
        arr.push(newYear);
        arr.shift();
        setYears(arr);
        setIteration(!iteration);
    }

    const nextYearMonths = () => {
        let nextYear = yearMonths + 1;
        setYearMonths(nextYear);
        setIteration(!iteration);
    }

    const previousYearMonths = () => {
        let nextYear = yearMonths - 1;
        setYearMonths(nextYear);
        setIteration(!iteration);
    }

    const nextYearMonthsWeek = () => {
        if(yearMonthsweek === 11) {
            setYearMonthsweek(0);
            nextYearMonths();
        } else
        {
            let nextMonth = yearMonthsweek + 1;
            setYearMonthsweek(nextMonth);
            setIteration(!iteration);
        }
    }

    const previousYearMonthsWeek = () => {
        if(yearMonthsweek === 0) {
            setYearMonthsweek(11);
            previousYearMonths();
        } else {
        let nextMonth = yearMonthsweek - 1;
        setYearMonthsweek(nextMonth);
        setIteration(!iteration);
    }
    }

    const backYear = () => {
        let arr = years;
        let newYear = arr[0] - 1;
        arr.unshift(newYear);
        arr.pop();
        setYears(arr);
        setIteration(!iteration);
    }

    return (
        <>
        {selectType === "year" ?  <div className='barchart'>
            <div className="button-container">
                <button className='button_report_selected' onClick={()=>{setSelectType("year")}}>Yearly</button>
                <button className='button_report' onClick={()=>{setSelectType("month")}}>Monthly</button>
                <button className='button_report' onClick={()=>{setSelectType("week")}}>Weekly</button>
            </div>
            <div className='move_bars'>
                <MdArrowBackIosNew size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={()=>{backYear()}}/>
                All Years
                <MdArrowForwardIos size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={()=>{nextYear()}}/>
            </div>
            <Bar data={data} options={options} />

        </div> : selectType === "month" ? <div className='barchart'>
            <div className="button-container">
                <button className='button_report' onClick={()=>{setSelectType("year")}}>Yearly</button>
                <button className='button_report_selected' onClick={()=>{setSelectType("month")}}>Monthly</button>
                <button className='button_report' onClick={()=>{setSelectType("week")}}>Weekly</button>
            </div>
            <div className='move_bars'>
                <MdArrowBackIosNew size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={()=>{previousYearMonths()}}/>
                {yearMonths}
                <MdArrowForwardIos size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={()=>{nextYearMonths()}}/>
            </div>
            <Bar data={dataMonths} options={options} />

        </div> :
            <div className='barchart'>
                <div className="button-container">
                    <button className='button_report' onClick={()=>{setSelectType("year")}}>Yearly</button>
                    <button className='button_report' onClick={()=>{setSelectType("month")}}>Monthly</button>
                    <button className='button_report_selected' onClick={()=>{setSelectType("week")}}>Weekly</button>
                </div>
                <div className='move_bars'>
                    <MdArrowBackIosNew size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={()=>{previousYearMonthsWeek()}}/>
                    {months[yearMonthsweek]} {yearMonths}
                    <MdArrowForwardIos size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={()=>{nextYearMonthsWeek()}}/>
                </div>
                <Bar data={dataweeks} options={options} />

            </div>
        }
            </>

    )
}

export default BarChart