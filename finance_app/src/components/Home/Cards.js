import React, { useState, useEffect } from "react";

function Cards(props) {
  const [totalIncomes, setTotalIncomes] = useState(0);
  const [totalExpenses, setTotalExpenses] = useState(0);
  const [profit, setProfit] = useState(0);

  const filterTrans = () => {
    console.log("bruh");
    let arr = [];
    props.transData.transactions.filter((item) => {
      if (item.date.includes(props.year)) {
        arr.push(item);
      }
    });

    let totalIncome = 0;
    let totalExpense = 0;

    arr.forEach((item) => {
      if (item.subcategory.category.type === "expense") {
        totalExpense += item.amount / item.currency.rate;
      } else {
        totalIncome += item.amount / item.currency.rate;
      }
    });
    setTotalExpenses(totalExpense.toFixed(2));
    setTotalIncomes(totalIncome.toFixed(2));
    setProfit((totalIncome - totalExpense).toFixed(2));
  };

  useEffect(() => {
    filterTrans();
  }, [props.year]);

  return (
    <div className="container-cardss">
      <div className="cardss" style={{ background: "#2684FF" }}>
        <h1 style={{ color: "#eee" }}>Incomes</h1>

        <div className="numberr">
          <p style={{ textAlign: "center", color: "#eee" }}>{totalIncomes}$</p>
        </div>
      </div>
      <div className="cardss">
        <div className="cardss-i">
          <h1>Profit</h1>

          <div className="numberr">
            <p style={{ textAlign: "center" }}>{profit}$</p>
          </div>
        </div>
      </div>
      <div className="cardss" style={{ background: "#fa3425" }}>
        <div className="cardss-i">
          <h1 style={{ color: "#eee" }}>Expenses</h1>

          <div className="numberr">
            <p style={{ textAlign: "center", color: "#eee" }}>
              {totalExpenses}$
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Cards;
