import React, { useState } from 'react';
import ExpenseChart from './ExpenseChart';
import IncomeChart from './IncomeChart';
import { MdArrowForwardIos, MdArrowBackIosNew } from 'react-icons/md'

function PieCharts(props) {
    const [selectType, setSelectType] = useState("year");
    const [year, setYear] = useState(2020);
    const [iteration, setIteration] = useState(false);
    const [month, setMonth] = useState(1);
    const [week, setWeek] = useState(1);
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const weeks = ["Week_1", "Week_2", "Week_3", "Week_4"];

    ////// Adds 1 to the year //////////////////////////////////////////////////////////////////////////////
    const nextYear = () => {
        let newyear = year + 1;
        setYear(newyear);
        setIteration(!iteration);
    }

    ////// removes 1 to the year //////////////////////////////////////////////////////////////////////////////
    const previousYear = () => {
        let newyear = year - 1;
        setYear(newyear);
        setIteration(!iteration);
    }

    ////// Adds 1 to the month //////////////////////////////////////////////////////////////////////////////
    const nextMonth = () => {
        if (month === 12) {
            setMonth(1);
            let newyear = year + 1;
            setYear(newyear);
            setIteration(!iteration)
        } else {
            let newMonth = month + 1;
            setMonth(newMonth);
            setIteration(!iteration);
        }
    }

    ////// removes 1 to the month //////////////////////////////////////////////////////////////////////////////
    const previousMonth = () => {
        if (month === 1) {
            setMonth(12);
            let newyear = year - 1;
            setYear(newyear);
            setIteration(!iteration)
        } else {
            let newMonth = month - 1;
            setMonth(newMonth);
            setIteration(!iteration);
        }
    }

    ////// Adds 1 to the week //////////////////////////////////////////////////////////////////////////////
    const nextWeek = () => {
        if (week === 4) {
            setWeek(1);
            let newMonth = month + 1;
            setMonth(newMonth);
            if (month === 12) {
                setMonth(1);
                let newyear = year + 1;
                setYear(newyear);
                setIteration(!iteration)
            } else {
                let newMonth = month + 1;
                setMonth(newMonth);
                setIteration(!iteration);
            }
        } else {
            let newWeek = week + 1;
            setWeek(newWeek);
            setIteration(!iteration);
        }
    }

     ////// Adds 1 to the week //////////////////////////////////////////////////////////////////////////////
     const previousWeek = () => {
        if (week === 1) {
            setWeek(4);
            let newMonth = month - 1;
            setMonth(newMonth);
            if (month === 1) {
                setMonth(12);
                let newyear = year - 1;
                setYear(newyear);
                setIteration(!iteration)
            } else {
                let newMonth = month - 1;
                setMonth(newMonth);
                setIteration(!iteration);
            }
        } else {
            let newWeek = week - 1;
            setWeek(newWeek);
            setIteration(!iteration);
        }
    }

    return (
        <>
            {selectType === 'year' ?
                <div className='pie_charts_container' >
                    <div className="button-container">
                        <button className='button_report_selected' onClick={() => { setSelectType("year") }}>Yearly</button>
                        <button className='button_report' onClick={() => { setSelectType("month") }}>Monthly</button>
                        <button className='button_report' onClick={() => { setSelectType("week") }}>Weekly</button>
                    </div>
                    <div className='move_bars'>
                        <MdArrowBackIosNew size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={() => { previousYear() }} />
                        {year}
                        <MdArrowForwardIos size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={() => { nextYear() }} />
                    </div>
                    <div className='pie_charts'>
                        <IncomeChart transData={props.transData} catData={props.catData} year={year} month={null} week={null} iteration={iteration} selectType={selectType} />
                        <ExpenseChart transData={props.transData} catData={props.catData} year={year} month={null} week={null} iteration={iteration} selectType={selectType} />
                    </div>
                </div> : selectType === "month" ?
                    <div className='pie_charts_container' >
                        <div className="button-container">
                            <button className='button_report' onClick={() => { setSelectType("year") }}>Yearly</button>
                            <button className='button_report_selected' onClick={() => { setSelectType("month") }}>Monthly</button>
                            <button className='button_report' onClick={() => { setSelectType("week") }}>Weekly</button>
                        </div>
                        <div className='move_bars'>
                            <MdArrowBackIosNew size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={() => { previousMonth() }} />
                            {months[month - 1]} {year}
                            <MdArrowForwardIos size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={() => { nextMonth() }} />
                        </div>
                        <div className='pie_charts'>
                            <IncomeChart transData={props.transData} catData={props.catData} year={year} month={month} week={null} iteration={iteration} selectType={selectType} />
                            <ExpenseChart transData={props.transData} catData={props.catData} year={year} month={month} week={null} iteration={iteration} selectType={selectType} />
                        </div>
                    </div> :
                    <div className='pie_charts_container' >
                        <div className="button-container">
                            <button className='button_report' onClick={() => { setSelectType("year") }}>Yearly</button>
                            <button className='button_report' onClick={() => { setSelectType("month") }}>Monthly</button>
                            <button className='button_report_selected' onClick={() => { setSelectType("week") }}>Weekly</button>
                        </div>
                        <div className='move_bars'>
                            <MdArrowBackIosNew size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={() => { previousWeek() }} />
                            {weeks[week - 1]} {months[month - 1]} {year}
                            <MdArrowForwardIos size="1.8rem" cursor="pointer" className='move_bar_btn' onClick={() => { nextWeek() }} />
                        </div>
                        <div className='pie_charts'>
                            <IncomeChart transData={props.transData} catData={props.catData} year={year} month={month} week={week} iteration={iteration} selectType={selectType} />
                            <ExpenseChart transData={props.transData} catData={props.catData} year={year} month={month} week={week} iteration={iteration} selectType={selectType} />
                        </div>
                    </div>
            }
        </>

    )
}

export default PieCharts