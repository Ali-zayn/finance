import React, { useState, useEffect } from "react";
import ProgressBar from "../Profit_bar/Profit_bar";

const GoalBar = ({
  data,
  setYear,
  percent,
  usedGoal,
  year,
  filterGoal,
  filterTrans,
  Calcpercent,
}) => {
  const testData = [{ bgcolor: "#4486FF", completed: percent }];

  useEffect(() => {
    filterGoal();
    filterTrans();
    Calcpercent();
  }, [year, usedGoal]);
  return (
    <>
      <div className="GoalbarContainer">
        <div className="p">
          <b>Goal : </b> {usedGoal.amount}$
        </div>
        <div className="App">
          {testData.map((item, idx) => (
            <ProgressBar
              key={idx}
              bgcolor={item.bgcolor}
              completed={item.completed}
            />
          ))}
        </div>
        <div className="selecty">
          <select
            className="role_list"
            onChange={(e) => {
              setYear(parseInt(e.target.value));
            }}
          >
            {data.map((item) => (
              <option value={item.year}>{item.year}</option>
            ))}
          </select>
        </div>
      </div>
    </>
  );
};

export default GoalBar;
