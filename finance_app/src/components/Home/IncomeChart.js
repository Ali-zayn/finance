import React, { useState, useEffect } from 'react';
import { Chart, ArcElement, Title, Legend, Tooltip } from 'chart.js';
import './Home.css';
import { Doughnut } from 'react-chartjs-2';
Chart.register(ArcElement, Title, Tooltip, Legend);

function ExpenseChart(props) {
    const [filteredTrans, setFilteredTrans] = useState([]);
    const [filteredTransMonth, setFilteredTransMonth] = useState([]);
    const [filteredTransWeek, setFilteredTransWeek] = useState([]);
    const [filteredCats, setFilteredCats] = useState([]);
    const [categories, setCategories] = useState([]);
    const [amounts, setAmounts] = useState([]);
    const [colors, setColors] = useState([]);
    const [iteration, setIteration] = useState(true);
    let data = {
        labels: categories,
        datasets: [{
            data: amounts,
            backgroundColor: colors,
            hoverOffset: 4
        }]
    }

    const Iterate = () => {
        setIteration(props.iteration);
    }

    //////// Filter Transactions By Year /////////////////////////////////////////////////////////////////////////////////////////
    const filterTrans = () => {
        console.log("bruh");
        let arr = [];
        props.transData.transactions.filter(item => {
            if (item.date.includes(props.year)) {
                if (item.subcategory.category.type.includes("income"))
                    arr.push(item);
            }
        })
        setFilteredTrans(arr);
    }

    ////////// Filter transactions By Months ///////////////////////////////////////////////////////////////////////////////
    const filterTransMonths = () => {
        if (props.month !== null) {
            let arr = [];

            props.transData.transactions.filter(item => {
                if (item.date.includes(props.year)) {
                    if (parseInt(item.date.split('-')[1]) === props.month) {
                        if (item.subcategory.category.type.includes("income")) {
                            arr.push(item);
                        }
                    }
                }
            })
            setFilteredTransMonth(arr);
        }

    }

    ////////// Filter transactions By Weeks ///////////////////////////////////////////////////////////////////////////////
    const filterTransWeek = () => {
        if (props.week !== null) {
            let arr = [];

            props.transData.transactions.filter(item => {
                if (item.date.includes(props.year)) {
                    if (parseInt(item.date.split('-')[1]) === props.month) {
                        if (props.week === 1) {
                            if (parseInt(item.date.split('-')[2]) >= 1 && parseInt(item.date.split('-')[2]) <= 7) {
                                if (item.subcategory.category.type.includes("income")) {
                                    arr.push(item);
                                }
                            }
                        } else if (props.week === 2) {
                            if (parseInt(item.date.split('-')[2]) >= 8 && parseInt(item.date.split('-')[2]) <= 14) {
                                if (item.subcategory.category.type.includes("income")) {
                                    arr.push(item);
                                }
                            }
                        } else if (props.week === 3) {
                            if (parseInt(item.date.split('-')[2]) >= 15 && parseInt(item.date.split('-')[2]) <= 21) {
                                if (item.subcategory.category.type.includes("income")) {
                                    arr.push(item);
                                }
                            }
                        } else if (props.week === 4) {
                            if (parseInt(item.date.split('-')[2]) >= 22 && parseInt(item.date.split('-')[2]) <= 31) {
                                if (item.subcategory.category.type.includes("income")) {
                                    arr.push(item);
                                }
                            }
                        }
                    }
                }
            })
            setFilteredTransWeek(arr);
        }

    }

    ///////////// Get only Income Categories ////////////////////////////////////////////////////////////////////////////////////
    const getCategories = () => {
        let arr = [];
        props.catData.subCat.filter(item => {
            if (item.type.includes("income")) {
                arr.push(item);
            }
        })
        setFilteredCats(arr);

        let arr2 = [];
        filteredCats.forEach(item => {
            arr2.push(item.name);
        });
        setCategories(arr2);

        Calculate();
        putColors();
    }

    const Calculate = () => {
        let arr3 = [];
        filteredCats.forEach(item => {
            let amount = 0;
            if (props.week === null && props.month === null) {
                filteredTrans.filter(Item => {
                    if (Item.subcategory.category.name.includes(item.name)) {
                        amount += Item.amount / Item.currency.rate;
                    }
                })
                arr3.push(amount.toFixed(2));
            } else if (props.month !== null && props.week === null) {
                filteredTransMonth.filter(Item => {
                    if (Item.subcategory.category.name.includes(item.name)) {
                        amount += Item.amount / Item.currency.rate;
                    }
                })
                arr3.push(amount.toFixed(2));
            } else {
                filteredTransWeek.filter(Item => {
                    if (Item.subcategory.category.name.includes(item.name)) {
                        amount += Item.amount / Item.currency.rate;
                    }
                })
                arr3.push(amount.toFixed(2));
            }
        })
        setAmounts(arr3);
    }

    const putColors = () => {
        let arr = [];
        let index = 255;
        filteredCats.forEach(() => {
            let color = `rgb(50, 50, ${index})`;
            arr.push(color);
            index -= 30;
        })
        setColors(arr);
    }

    useEffect(() => {
        filterTrans();
        getCategories();
        Iterate();
        filterTransMonths();
        filterTransWeek();
    }, [iteration, props.iteration, props.selectType]);

    return (
        <div className='chart'>

            <Doughnut data={data} />
            <h4>Incomes</h4>
        </div>
    )
}

export default ExpenseChart