import React, { useState, useEffect } from "react";
import { Chart, ArcElement, Title, Legend, Tooltip } from "chart.js";
import "./Home.css";
import Sidebar from "../Sidebar/Sidebar";
import LoadingEffect from "../LoadingEffect/LoadingEffect";
import axios from "axios";
import ProgressBar from "../Profit_bar/Profit_bar";
import BarChart from "./Barchart";
import ExpenseChart from "./ExpenseChart";
import IncomeChart from "./IncomeChart";
import GoalBar from "./GoalBar";
import Cards from "./Cards";
import PieCharts from "./PieCharts";
import { FaYammer } from "react-icons/fa";

Chart.register(ArcElement, Title, Tooltip, Legend);

function Home() {
  const [transData, setTransData] = useState(null);
  const [catData, setCatdata] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loading1, setLoading1] = useState(true);
  const [loadingCat, setLoadingCat] = useState(true);
  const [error, setError] = useState(null);
  const [data, setData] = useState(null);
  const [year, setYear] = useState(null);
  const [usedGoal, setUsedGoal] = useState({});
  const [trans, setTrans] = useState([]);
  const [amount, setAmount] = useState(0);
  const [percent, setPercent] = useState(0);



  setTimeout(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      window.location.href = "/login";
    }
  }, 10);

  useEffect(() => {
    FetchData();
    FetchCategories();
    FetchGoals();

  }, []);

  // Get All Goals ///////////////////////////////////////////////////////////////////////////////
  const FetchGoals = () => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}api/profit_goals`, {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setData(data);
        setYear(data[0].year);
        setLoading1(false);
      })
      .catch((error) => {
        console.error(error.message);
        setError(error);
      });
  };

  // Get All Transactions ///////////////////////////////////////////////////////////////////////////////
  const FetchData = async () => {
    setLoading(true);
    await axios
      .get(`${process.env.REACT_APP_BACKEND_URL}api/transaction`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        setTransData(res.data);
        setLoading(false);
      })

      .catch((error) => {
        setError(error);
        console.error(error.message);
      });
  };

  const filterGoal = () => {
    data.filter((item) => {
      if (item.year === year) {
        setUsedGoal(item);
      }
    });
  };

  const Calcpercent = () => {
    let perc = 0;

    perc = Math.trunc((amount / usedGoal.amount) * 100);
    setPercent(perc);

  };

  const filterTrans = () => {
    let arr = [];
    transData.transactions.filter((item) => {
      if (item.date.includes(year)) {

        arr.push(item);
      }
    });
    let amountTrans = 0;
    let totalAmount = 0;
    arr.forEach((item) => {
      if (item.subcategory.category.type === "expense") {
        amountTrans = (item.amount / item.currency.rate) * -1;
      } else {
        amountTrans = item.amount / item.currency.rate;
      }
      totalAmount += amountTrans;
    });

    setAmount(totalAmount.toFixed(2));
  };

  /// GET All Categories ////////////////////////////////////////////////////////////////////////
  const FetchCategories = () => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}api/category`, {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setCatdata(data);
        setLoadingCat(false);
      })
      .catch((error) => {
        console.error(error.message);
        setError(error);
      });
  };

  if (loading || loadingCat || loading1) return <LoadingEffect />;
  if (error) return "Error";
  return (
    <div className="container">
      <Sidebar />
      <div className="home_wrapper">
        <Cards transData={transData} year={year} />
        <GoalBar
          data={data}
          transData={transData}
          year={year}
          setYear={setYear}
          usedGoal={usedGoal}
          percent={percent}
          filterTrans={filterTrans}
          Calcpercent={Calcpercent}
          filterGoal={filterGoal}
        />
        <PieCharts transData={transData} catData={catData} />
        <div className="barchart_container">
          <BarChart transData={transData} catData={catData} />

        </div>
      </div>
    </div>
  );
}

export default Home;
