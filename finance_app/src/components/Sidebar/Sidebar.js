import React, {useState} from 'react';
import './Sidebar.css';
import {Link} from 'react-router-dom';
import {AiOutlineHome} from 'react-icons/ai'
import {RiAdminLine} from 'react-icons/ri'
import {BiTargetLock} from 'react-icons/bi'
import {FaLayerGroup, FaRegMoneyBillAlt} from 'react-icons/fa'
import {BiInfoCircle} from 'react-icons/bi'
import {HiMenu} from 'react-icons/hi'
import {BiLogOut} from 'react-icons/bi'
import {MdAttachMoney} from 'react-icons/md'

function Sidebar() {
    const [active, setActive] = useState('hidden')

    const Log_out = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        localStorage.removeItem('id');
      }

    return (
        
        <div className="sidebar">
            <div className='menu'>
            <HiMenu size='2rem' onClick={()=>{active === 'show' ? setActive('hidden') : setActive('show')}}/>
            </div>
            <div className={active}>
            <img src='/images/logo.svg' alt='logo' className='logo_image'/>
            <Link to='/home' className='a'><AiOutlineHome/> <p>Dashboard</p></Link>
            <Link to='/admins' className='a'><RiAdminLine/>  <p>Admins</p></Link>
            <Link to='/goals' className='a'><BiTargetLock/> <p>Goals</p></Link>
            <Link to='/categories' className='a'><FaLayerGroup/> <p>Categories</p></Link>
            <Link to='/login_info' className='a'><BiInfoCircle/> <p>Profile</p></Link>
            <Link to='/currency' className='a'><MdAttachMoney/> <p>Currency</p></Link>
            <Link to='/transactions' className='a'><FaRegMoneyBillAlt/> <p>Transactions</p></Link>
            </div>
            <Link to='/login' className='a logout' onClick={Log_out}><BiLogOut size='1.5rem' padding-left='20px'/></Link>
        </div>


    )
}

export default Sidebar