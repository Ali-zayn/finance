import {React, useState, useEffect} from 'react';
import './card-admin.css'
import {FaUserEdit} from "react-icons/fa";
import {MdDelete} from 'react-icons/md';
import Popup from '../Popup/Popup';
import {MdOutlineAirplanemodeActive, MdOutlineAirplanemodeInactive} from "react-icons/md"

function Cardadmin(props) {

    return (
        <>
            <div className="container-admin">
                <div className="box">
                    <div className="image">
                        <img src={`${process.env.REACT_APP_BACKEND_URL}pictures/${props.picture}`}/>
                    </div>
                    <div className="fix-name">
                        <div className="name_job">{props.f_name}</div>
                        <div className="name_job">{props.l_name}</div>
                    </div>
                    <div className="role">
                        {props.role === 1 ? <h5>Super Admin</h5> : <h5>Admin</h5>}

                    </div>
                    <div className="email-admin">
                        <h5>{props.email}</h5>
                    </div>
                    <div className="phone-admin">
                        <h5>{props.phone}</h5>
                    </div>
                    {props.Role === "superAdmin" && props.id !== parseInt(props.ID) ?
                        <div className="btns">
                            <button className='edit point' onClick={() => {
                                props.setAdminID(props.id);
                                props.setPopup1(true);
                                props.setUsername(props.username);
                                props.setRoleID(props.role);
                                props.setFname(props.f_name);
                                props.setLname(props.l_name);
                                props.setEmail(props.email);
                                props.setPhone(props.phone);
                                props.setPicture(props.picture);
                                props.setResetPass(0);

                            }}>
                                <FaUserEdit size='1.2rem' color='white'/></button>
                            {props.active === false ?
                                <button className='trash point' onClick={() => {
                                    props.setPopup(true);
                                    props.setAdminID(props.id);
                                    props.setActive(props.active);
                                    props.setPasswordBackup(props.password);
                                }}>

                                    <MdOutlineAirplanemodeInactive
                                        size='1.2rem' color='white' className="point"/></button> :
                                <button className='view point' onClick={() => {
                                    props.setPopup(true);
                                    props.setAdminID(props.id);
                                    props.setActive(props.active);
                                }}>
                                    <MdOutlineAirplanemodeActive
                                        size='1.2rem' color='white' className="point"/></button>}

                        </div> : ""}
                </div>
            </div>
            {/*/// EDIT Admin Popup /////////////////////////////////////////////////////////////////////////////////////////////*/}
        </>
    )
}

export default Cardadmin