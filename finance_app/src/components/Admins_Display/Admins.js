import { React, useState, useEffect } from 'react';
import './Admins.scss';
import Sidebar from '../Sidebar/Sidebar';
import Cardadmin from "./Card-admin";
import LoadingEffect from "../LoadingEffect/LoadingEffect";
import { BsCloudPlus } from 'react-icons/bs';
import { IoMdPersonAdd } from 'react-icons/io'
import Popup from "../Popup/Popup";
import { FaUserEdit } from "react-icons/fa";
import axios from "axios";
import { AiOutlineSearch } from 'react-icons/ai'
import { HiPlus } from "react-icons/hi";


function Admins() {
    const [Loading, setLoading] = useState(true);
    const [Loading1, setLoading1] = useState(true);
    const [data, setData] = useState(null);
    const [rolesData, setRolesData] = useState(null);
    const [error, setError] = useState(null);
    const [adminID, setAdminID] = useState(null);
    const [active, setActive] = useState(null);
    const [username, setUsername] = useState(null);
    const [fname, setFname] = useState(null);
    const [addfname, setAddfname] = useState(null);
    const [addlname, setAddlname] = useState(null);
    const [picture, setPicture] = useState([]);
    const [addemail, setAddemail] = useState(null);
    const [number, setNumber] = useState(null);
    const [lname, setLname] = useState(null);
    const [email, setEmail] = useState(null);
    const [phone, setPhone] = useState(null);
    const [password, setPassword] = useState(null);
    const [passwordBackup, setPasswordBackup] = useState(null);
    const [conf, setConf] = useState(null);
    const [usernameAdd, setUsernameAdd] = useState(null);
    const [passwordAdd, setPasswordAdd] = useState(null);
    const [confAdd, setConfAdd] = useState(null);
    const [roleIDAdd, setRoleIDAdd] = useState(2);
    const [roleID, setRoleID] = useState(null);
    const [popup, setPopup] = useState(false);
    const [popup1, setPopup1] = useState(false);
    const [popup2, setPopup2] = useState(false);
    const [resetPass, setResetPass] = useState(0)
    const [searchrterm, setSearchTerm] = useState("");

    const onChangeFile = e => {
        setPicture(e.target.files[0]);
    }

    const token = localStorage.getItem("token");
    if (!token) {
        window.location.href = "/login";
    }
    const Role = localStorage.getItem("role");
    const ID = localStorage.getItem("id");

    useEffect(() => {
        FetchData();
        FetchRolesData();
    }, []);

    // Get All Admins ///////////////////////////////////////////////////////////////////////////////
    const FetchData = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/admins`, {
            method: "GET",
            headers: { "Content-Type": "application/json" },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setData(data);
                setResetPass(0);
                setLoading(false);
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    };

    // Get All Roles ///////////////////////////////////////////////////////////////////////////////
    const FetchRolesData = () => {
        setLoading1(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/roles`, {
            method: "GET",
            headers: { "Content-Type": "application/json" },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setRolesData(data);
                setResetPass(0);
                setLoading1(false);
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    };

    // Edit Admin //////////////////////////////////////////////////////////////////////////////
    const EditAdmin = async () => {
        if (username.length >= 6) {
                if (password === conf) {
                    console.log(typeof (picture));
                    setLoading(true);
                    const formData = new FormData();
                    formData.append("username", username);
                    formData.append("f_name", fname);
                    formData.append("l_name", lname);
                    { password === null || password === "null" || password.length === 0 ? formData.append("password", passwordBackup) : formData.append("password", password) }
                    formData.append("email", email);
                    formData.append("phoneNumber", phone);
                    if (typeof (picture) === "array" || typeof (picture) === "object") {
                        formData.append("picture", picture);
                    }
                    formData.append("slug", username);
                    formData.append("role_id", roleID);
                    formData.append("_method", "PUT");
                    await axios.post(`${process.env.REACT_APP_BACKEND_URL}api/admins/${adminID}`, formData, {
                        headers: { "Content-Type": "multipart/form-data" },
                    })
                        .then((res) => {
                            console.log("updated");
                            setLoading(false);
                            FetchData();
                        })

                        .catch((error) => {
                            console.log(error);
                        });
                } else {
                    return alert('Please confirm the password correctly');
                }
        } else {
            alert("The uswername must be at least 6 characters long.");
        }
    }

    // Change Activation ///////////////////////////////////////////////////////////////////////////
    const ChangeActivation = async () => {
        if (active === true) {
            setLoading(true);
            await fetch(`${process.env.REACT_APP_BACKEND_URL}api/admins/${adminID}?isActive=0`, {
                method: "PUT",
                headers: { "Content-Type": "application/json" },
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
                .then(() => {
                    setLoading(false);
                    setActive(null);
                    setAdminID(null);
                    FetchData();
                })
                .catch((error) => {
                    console.error(error.message);
                    setError(error);
                });
        } else if (active === false) {
            setLoading(true);
            fetch(`${process.env.REACT_APP_BACKEND_URL}api/admins/${adminID}?isActive=1`, {
                method: "PUT",
                headers: { "Content-Type": "application/json" },
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    }
                    throw response;
                })
                .then(() => {
                    setLoading(false);
                    setActive(null);
                    setAdminID(null);
                    FetchData();
                })
                .catch((error) => {
                    console.error(error.message);
                    setError(error);
                });
        }
    }

    //// ADD Admin /////////////////////////////////////////////////////////////////////////////////////////////////////// 
    const AddAdmin = () => {
        if (usernameAdd.length >= 6) {
            if (passwordAdd.length >= 6) {
                if (passwordAdd === confAdd) {
                    setLoading(true);

                    const formData = new FormData();
                    formData.append("username", usernameAdd);
                    formData.append("f_name", addfname);
                    formData.append("l_name", addlname);
                    formData.append("password", passwordAdd);
                    formData.append("email", addemail);
                    formData.append("phoneNumber", number);
                    formData.append("picture", picture);
                    formData.append("slug", usernameAdd);
                    formData.append("role_id", roleIDAdd);
                    formData.append("isActive", 1);
                    axios.post(`${process.env.REACT_APP_BACKEND_URL}api/admins`, formData, {
                        headers: { "Content-Type": "multipart/form-data" },
                    })
                        .then((res) => {
                            console.log("sent");
                            setLoading(false);
                            FetchData();
                        })
                        .catch((error) => {
                            console.log(error.message);


                        });
                } else {
                    return alert('Please confirm the password correctly');
                }
            } else {
                alert("The password must be at least 6 characters long.");
            }
        } else {
            alert("The uswername must be at least 6 characters long.");
        }
    }


    if (Loading || Loading1) return (
        <div>
            <Sidebar />
            <LoadingEffect />
        </div>

    )

    return (
        <div className='container'>
            <Sidebar />
            <div className='admin-wrapper'>
                <h1>Admins</h1>


                <form action="" className="search-bar">
                    <input type="search" name="search" pattern=".*\S.*"
                        onChange={(e) => {
                            setSearchTerm((e.target.value))
                        }}
                        required />
                    <button className="search-btn" type="submit">
                        <span>Search</span>
                    </button>
                </form>

                {/*onChange={(e)=>{setSearchTerm((e.target.value))}}*/}

                {Role === "superAdmin" ?
                    <button className='plus-butn point' onClick={() => {
                        setPopup2(true);
                        setUsernameAdd(null)
                        setPasswordAdd(null)
                        setAddfname(null)
                        setAddlname(null)
                        setAddemail(null)
                        setConfAdd(null);
                        setNumber(null)
                    }}>


                        <HiPlus size='2rem' />
                    </button> : ""}


                <div className="card-admin-wrapper">
                    {data.admins.filter((item => {
                        if (searchrterm === "") {
                            return item;
                        } else if (item.f_name.toLowerCase().includes(searchrterm.toLowerCase()) || item.l_name.toLowerCase().includes(searchrterm.toLowerCase())) {
                            return item;
                        }
                    })).map((item, index) => {

                        return (
                            <Cardadmin id={item.id} username={item.username} role={item.role_id}
                                email={item.email} phone={item.phoneNumber} f_name={item.f_name}
                                l_name={item.l_name} password={item.password} picture={item.picture}
                                popup1={popup1}
                                setPopup1={setPopup1} popup={popup} setPopup={setPopup} setAdminID={setAdminID}
                                setUsername={setUsername}
                                setPassword={setPassword} ID={ID}
                                setConf={setConf} setRoleID={setRoleID} setFname={setFname} setLname={setLname}
                                setPhone={setPhone} Role={Role} setPicture={setPicture}
                                setEmail={setEmail} active={item.isActive} setActive={setActive}
                                setResetPass={setResetPass} setPasswordBackup={setPasswordBackup} />


                        );
                    })}
                </div>
            </div>

            {/* //// Edit Admin Popup //////////////////////////////////////////////////////////////////////////// */}

            <Popup trigger={popup1} setTrigger={setPopup1}>


                <div className="popup_title">
                    <FaUserEdit size="1.5rem" />
                    <h3>Edit Admin</h3>
                </div>
                <form onSubmit={() => {
                    EditAdmin();
                    setPopup1(false);
                }}>
                    <div className="form-group">
                        <div className='form_admin'>
                            <div className='popup_left'>
                                <b><label for="fname">First name</label></b>
                                <input
                                    type="text"
                                    name="fname"
                                    id="fname"
                                    placeholder="First-name"
                                    className='admin_input'
                                    value={fname}
                                    onChange={(e) => {
                                        setFname(e.target.value);
                                    }}
                                />
                                <b><label htmlFor="lname">Last name</label></b>
                                <input
                                    type="text"
                                    name="lname"
                                    id="lname"
                                    placeholder="Last-name"
                                    className='admin_input'
                                    value={lname}
                                    onChange={(e) => {
                                        setLname(e.target.value);
                                    }}
                                />
                                <b><label htmlFor="email">Email</label></b>
                                <input
                                    type="text"
                                    name="email"
                                    id="email"
                                    placeholder="enter email"
                                    className='admin_input'
                                    value={email}
                                    onChange={(e) => {
                                        setEmail(e.target.value);
                                    }}
                                />
                                <b><label htmlFor="phone">Phone number</label></b>
                                <input
                                    type="text"
                                    name="phone"
                                    id="phone"
                                    placeholder="enter phone-number"
                                    className='admin_input'
                                    value={phone}
                                    onChange={(e) => {
                                        setPhone(e.target.value);
                                    }}
                                />
                            </div>
                            <div className='popup_right'>
                                <b><label htmlFor="name">Username</label></b>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="Username"
                                    className='admin_input'
                                    value={username}
                                    onChange={(e) => {
                                        setUsername(e.target.value)
                                    }}
                                />
                                {resetPass === 0 ? <button onClick={() => {
                                    setResetPass(1)
                                }} className='reset_pass_btn'>Reset Password</button> :
                                    <div className='reset_pass'>
                                        <b><label htmlFor="password">Password</label></b>

                                        <input
                                            type="text"
                                            name="password"
                                            id="password"
                                            placeholder="Password"
                                            className='admin_input'
                                            value={password}
                                            onChange={(e) => {
                                                setPassword(e.target.value)
                                            }}
                                        />
                                        <b><label htmlFor="conf_password">Confirm Password</label></b>
                                        <input
                                            type="text"
                                            name="conf_password"
                                            id="conf_password"
                                            placeholder="Confirm Password"
                                            className='admin_input'
                                            value={conf}
                                            onChange={(e) => {
                                                setConf(e.target.value)
                                            }}
                                        /></div>}
                                <h4>Role:</h4>
                                <div className='role_list_wrapper'>
                                    <select className='role_list' value={roleID} onChange={(e) => {
                                        setRoleID(e.target.value)
                                    }}>
                                        {rolesData.map(item =>
                                            <option value={item.id}>{item.name}</option>
                                        )}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input
                            type="file"
                            name="picture"
                            onChange={(onChangeFile)}

                        />
                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />
                    </div>
                </form>

            </Popup>

            {/*/////////////////////////////////////////Confirmation Popup ///////////////////////////////////////// */}
            <Popup trigger={popup} setTrigger={setPopup}>
                <h3>Are you sure you want to switch the activation?</h3><br />
                <button className='confirmation' onClick={() => {
                    ChangeActivation();
                    setPopup(false)
                }}>Yes
                </button>
            </Popup>

            {/*//// ADD Admin Popup //////////////////////////////////////////////////////////////////////////////////////*/}
            <Popup trigger={popup2} setTrigger={setPopup2}>

                <div className="popup_title">
                    <FaUserEdit size="1.5rem" />
                    <h3>Add Admin</h3>
                </div>
                <form onSubmit={() => {
                    AddAdmin();
                    setPopup2(false)
                }}>
                    <div className="form-group">
                        <div className='form_admin'>
                            <div className='popup_left'>
                                <b><label htmlFor="fname">First name</label></b>
                                <input
                                    type="text"
                                    name="fname"
                                    id="fname"
                                    placeholder="First-name"
                                    className='admin_input'
                                    value={addfname}
                                    onChange={(e) => {
                                        setAddfname(e.target.value);
                                    }}
                                    required
                                />
                                <b><label htmlFor="lname">Last name</label></b>
                                <input
                                    type="text"
                                    name="lname"
                                    id="lname"
                                    placeholder="Last-name"
                                    className='admin_input'
                                    value={addlname}
                                    onChange={(e) => {
                                        setAddlname(e.target.value);
                                    }}
                                    required
                                />
                                <b><label htmlFor="email">Email</label></b>
                                <input
                                    type="text"
                                    name="email"
                                    id="email"
                                    placeholder="enter email"
                                    className='admin_input'
                                    value={addemail}
                                    onChange={(e) => {
                                        setAddemail(e.target.value);
                                    }}
                                    required
                                />
                                <b><label htmlFor="phone">Phone number</label></b>
                                <input
                                    type="text"
                                    name="phone"
                                    id="phone"
                                    placeholder="enter phone-number"
                                    className='admin_input'
                                    value={number}
                                    onChange={(e) => {
                                        setNumber(e.target.value);
                                    }}
                                    required
                                />
                            </div>
                            <div className='popup_right'>
                                <b><label htmlFor="name">Username</label></b>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="Username"
                                    className='admin_input'
                                    value={usernameAdd}
                                    onChange={(e) => {
                                        setUsernameAdd(e.target.value)
                                    }}
                                    required
                                />
                                <b><label htmlFor="password">Password</label></b>

                                <input
                                    type="text"
                                    name="password"
                                    id="password"
                                    placeholder="Password"
                                    className='admin_input'
                                    value={passwordAdd}
                                    onChange={(e) => {
                                        setPasswordAdd(e.target.value)
                                    }}
                                    required
                                />
                                <b><label htmlFor="conf_password">Confirm Password</label></b>
                                <input
                                    type="text"
                                    name="conf_password"
                                    id="conf_password"
                                    placeholder="Confirm Password"
                                    className='admin_input'
                                    value={confAdd}
                                    onChange={(e) => {
                                        setConfAdd(e.target.value)
                                    }}
                                    required
                                />
                                <h4>Role:</h4>
                                <div className='role_list_wrapper'>
                                    <select className='role_list' onChange={(e) => {
                                        setRoleIDAdd(e.target.value)
                                    }}>
                                        {rolesData.map(item =>
                                            <option value={item.id}>{item.name}</option>
                                        )}
                                    </select></div>
                            </div>
                        </div>
                        <input
                            type="file"
                            namme="picture"
                            onChange={(onChangeFile)}
                            required
                        />
                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />
                    </div>
                </form>

            </Popup>


        </div>

    )
}

export default Admins;