import React, {useState, useEffect} from 'react';
import './Transactions.css';
import Sidebar from '../Sidebar/Sidebar';
import LoadingEffect from '../LoadingEffect/LoadingEffect';
import Popup from '../Popup/Popup';
import {BiEditAlt} from 'react-icons/bi';
import {MdDelete} from 'react-icons/md';
import {FiPlusCircle} from 'react-icons/fi';
import TransPagination from "./TransPagination";
import axios from 'axios';
import {HiPlus} from "react-icons/hi";

function Transactions() {

    const token = localStorage.getItem("token");
    if (!token) {
        window.location.href = "/login";
    }

    const [data, setData] = useState([]);
    const [subData, setSubData] = useState(null);
    const [currData, setCurrData] = useState(null);
    const [loading, setLoading] = useState(true);
    const [loading1, setLoading1] = useState(true);
    const [loading2, setLoading2] = useState(true);
    const [error, setError] = useState(null);
    const [popupAdd, setPopupAdd] = useState(false);
    const [editPopup, setEditPopup] = useState(false)
    const [popupCur, setPopupCur] = useState(false);
    const [transID, setTransID] = useState(null);
    const [titleAdd, setTitleAdd] = useState(null);
    const [descAdd, setDescAdd] = useState(null);
    const [amountAdd, setAmountAdd] = useState(null);
    const [frequencyAdd, setFrequencyAdd] = useState("fixed");
    const [dateAdd, setDateAdd] = useState(null);
    const [currencyAdd, setCurrencyAdd] = useState(null);
    const [subCatAdd, setSubCatAdd] = useState(null);
    const [startDateAdd, setStartDateAdd] = useState(null);
    const [endDateAdd, setEndDateAdd] = useState(null);
    const [titleEdit, setTitleEdit] = useState(null);
    const [descEdit, setDescEdit] = useState(null);
    const [amountEdit, setAmountEdit] = useState(null);
    const [dateEdit, setDateEdit] = useState(null);
    const [frequencyEdit, setFrequencyEdit] = useState(null);
    const [currencyEdit, setCurrencyEdit] = useState(null);
    const [subCatEdit, setSubCatEdit] = useState(null);
    const [startDateEdit, setStartDateEdit] = useState(null);
    const [endDateEdit, setEndDateEdit] = useState(null);

    const [searchrterm, setSearchTerm] = useState("");

    const [selects, setSelects] = useState(null);
    const [selectfreq, setSelectfreq] = useState(null);
    const [selectsub, setSelectsub] = useState(null);


    const [currentPage, setCurrentPage] = useState(1);
    const [transPerPage, setTransPerPage] = useState(5);
    // Get current transactions


    useEffect(() => {
        FetchData();
        FetchSubData();
        FetchCurrData();
    }, []);

    // Get All Transactions ///////////////////////////////////////////////////////////////////////////////
    const FetchData = async () => {
        setLoading(true);
        await axios.get(`${process.env.REACT_APP_BACKEND_URL}api/transaction`, {
            headers: {"Content-Type": "multipart/form-data"},
        })
            .then((res) => {
                setData(res.data);
                setLoading(false);
            })

            .catch((error) => {
                setError(error);
                console.error(error.message)
            });
    };

    // Get All Subcategories ///////////////////////////////////////////////////////////////////////////////
    const FetchSubData = async () => {
        setLoading1(true);
        await axios.get(`${process.env.REACT_APP_BACKEND_URL}api/subCategories`, {
            headers: {"Content-Type": "multipart/form-data"},
        })
            .then((res) => {
                setSubData(res.data);
                setSubCatAdd(res.data[0].id);
                setLoading1(false);
            })

            .catch((error) => {
                setError(error);
                console.error(error.message)
            });
    };

    // Get All Currencies ///////////////////////////////////////////////////////////////////////////////
    const FetchCurrData = async () => {
        setLoading2(true);
        await axios.get(`${process.env.REACT_APP_BACKEND_URL}api/currency`, {
            headers: {"Content-Type": "multipart/form-data"},
        })
            .then((res) => {
                setCurrData(res.data);
                setCurrencyAdd(res.data[0].id)
                setLoading2(false);
            })

            .catch((error) => {
                setError(error);
                console.error(error.message)
            });
    };

    // Add Transaction ///////////////////////////////////////////////////////////////////////////////
    const AddTransaction = async () => {
        setLoading(true);
        if (frequencyAdd === 'fixed') {
            await axios.post(`${process.env.REACT_APP_BACKEND_URL}api/transaction?title=${titleAdd}&description=${descAdd}&amount=${amountAdd}&currency_id=${currencyAdd}&Frequency=${frequencyAdd}&subcategory_id=${subCatAdd}&date=${dateAdd}&slug=${titleAdd}`, {
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then((res) => {
                    setTitleAdd(null);
                    setDescAdd(null);
                    setAmountAdd(null);
                    setCurrencyAdd(currData[0].id);
                    setFrequencyAdd("fixed");
                    setSubCatAdd(subData[0].id);
                    setDateAdd(null);
                    setStartDateAdd(null);
                    setEndDateAdd(null);
                    setLoading(false);
                    FetchData();
                })

                .catch((error) => {
                    setError(error);
                    console.error(error.message)
                });
        } else {
            await axios.post(`${process.env.REACT_APP_BACKEND_URL}api/transaction?title=${titleAdd}&description=${descAdd}&amount=${amountAdd}&currency_id=${currencyAdd}&Frequency=${frequencyAdd}&subcategory_id=${subCatAdd}&date=${startDateAdd}&slug=${titleAdd}&start_date=${startDateAdd}&end_date=${endDateAdd}`, {
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then((res) => {
                    setTitleAdd(null);
                    setDescAdd(null);
                    setAmountAdd(null);
                    setCurrencyAdd(currData[0].id);
                    setFrequencyAdd("fixed");
                    setSubCatAdd(subData[0].id);
                    setDateAdd(null);
                    setStartDateAdd(null);
                    setEndDateAdd(null);
                    setLoading(false);
                    FetchData();
                })

                .catch((error) => {
                    setError(error);
                    console.error(error.message)
                });
        }
    };
    //Edit transaction////////////////////////////////////////////////////////////////////////////////////
    const EditTransaction = async () => {
        setLoading(true);
        if (frequencyEdit === 'fixed') {
            await axios.put(`${process.env.REACT_APP_BACKEND_URL}api/transaction/${transID}?title=${titleEdit}&description=${descEdit}&amount=${amountEdit}&currency_id=${currencyEdit}&Frequency=${frequencyEdit}&subcategory_id=${subCatEdit}&date=${dateEdit}&slug=${titleEdit}`)
                .then((res) => {
                    setLoading(false);
                    FetchData();
                })

                .catch((error) => {
                    setError(error);
                    console.error(error.message)
                });
        } else {
            await axios.put(`${process.env.REACT_APP_BACKEND_URL}api/transaction/${transID}?title=${titleEdit}&description=${descEdit}&amount=${amountEdit}&currency_id=${currencyEdit}&Frequency=${frequencyEdit}&subcategory_id=${subCatEdit}&date=${dateEdit}&slug=${titleEdit}&start_date=${startDateEdit}&end_date=${endDateEdit}`)
                .then((res) => {
                    setLoading(false);
                    FetchData();
                })

                .catch((error) => {
                    setError(error);
                    console.error(error.message)
                });
        }
    };


    // Delete Transactions ///////////////////////////////////////////////////////////////////////////////
    const DeleteTrans = async () => {
        setLoading(true);
        await axios.delete(`${process.env.REACT_APP_BACKEND_URL}api/transaction/${transID}`, {
            headers: {"Content-Type": "multipart/form-data"},
        })
            .then((res) => {
                setLoading(false);
                FetchData();
            })

            .catch((error) => {
                setError(error);
                console.error(error.message)
            });
    };
    const indexOfLastTransaction = currentPage * transPerPage;
    const indexOfFirstTransaction = indexOfLastTransaction - transPerPage;
    // const currentTransaction = data.transactions.slice(indexOfFirstTransaction, indexOfLastTransaction);
    console.log(data.transactions);


    if (loading || loading1 || loading2) return <LoadingEffect/>;
    if (error) return "Error";
    console.log(searchrterm)

    return (
        <div className='container'>
            <Sidebar/>
            <div className='trans_wrapper'>
                <h1>Transactions</h1>
                <form action="" className="search-bar">
                    <input type="search" name="search" pattern=".*\S.*"
                           onChange={(e) => {
                               setSearchTerm((e.target.value))
                           }}
                           required/>
                    <button className="search-btn" type="submit">
                        <span>Search</span>
                    </button>
                </form>
                <button className='plus-butn point'>
                <HiPlus size='2rem' onClick={() => {
                    setPopupAdd(true)
                }}/>
                </button>
                <table className='styled-table trans_table'>
                    <thead className='info-table'>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Start-Date</th>
                        <th>End-Date</th>
                        <th>Amount</th>
                        <th>Currency</th>
                        <th><select className='trans_select' value={selects}
                                    onChange={(e) => setSelects(e.target.value)}>
                            <option>type</option>
                            <option>Income</option>
                            <option>Expense</option>
                        </select></th>

                        <th><select className='trans_select' value={selectsub} onChange={(e) => {
                            setSelectsub(e.target.value)
                        }}>
                            <option>Subcategory</option>
                            {subData.map(item =>
                                <option>{item.name}</option>
                            )}
                        </select>
                        </th>

                        <th><select className='trans_select' value={selectfreq} onChange={(e) => {
                            setSelectfreq(e.target.value)
                        }}>
                            <option>Frequency</option>
                            <option>Fixed</option>
                            <option>Recurrent</option>
                        </select></th>

                        <th>Edits</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.transactions.slice(indexOfFirstTransaction, indexOfLastTransaction)
                        .filter((item => {
                            if (searchrterm === "") {
                                return item;
                            } else if (item.date.toLowerCase().includes(searchrterm.toLowerCase())) {
                                return item;
                            }
                        }))
                        .filter(item => {
                        if (selects === null) {
                            return item;
                        } else if (selects === "type") {
                            return item;
                        } else if (item.subcategory.category.type.toLowerCase().includes(selects.toLowerCase())) {
                            return item;
                        }
                    }).filter(item => {
                        if (selectfreq === null) {
                            return item;
                        } else if (selectfreq === "Frequency") {
                            return item;
                        } else if (item.Frequency.toLowerCase().includes(selectfreq.toLowerCase())) {
                            return item;
                        }
                    }).filter(item => {
                        if (selectsub === null) {
                            return item;
                        } else if (selectsub === "Subcategory") {
                            return item;
                        } else if (item.subcategory.name.toLowerCase().includes(selectsub.toLowerCase())) {
                            return item;
                        }

                    }).map(item =>

                        <tr key={item.id}>
                            <td data-label="Title">{item.title}</td>

                            <td data-label="Description">
                                <div style={{
                                    "overflowY": "scroll",
                                    "height": "50px",
                                    "fontsize": "13px"
                                }}>{item.description}</div>
                            </td>
                            <td data-label="Date">{item.date}</td>
                            {item.start_date === null ? <td data-label="Start-Date">N/A</td> :
                                <td data-label="Start-Date">{item.start_date}</td>}
                            {item.end_date === null ? <td data-label="End-Date">N/A</td> :
                                <td data-label="End-Date">{item.end_date}</td>}
                            <td data-label="Amount">{item.amount}</td>
                            <td data-label="Currency">{item.currency.Symbol}</td>
                            <td data-label="Type">{item.subcategory.category.type}</td>
                            <td data-label="Subcategory">{item.subcategory.name}</td>
                            <td data-label="Frequency">{item.Frequency}</td>
                            <td data-label="Edits">
                                <div className='admin_edit_btns trans_edits'>
                                    <BiEditAlt
                                        size='1.5rem'
                                        color=""
                                        className='point'
                                        onClick={() => {
                                            setEditPopup(true);
                                            setTitleEdit(item.title);
                                            setAmountEdit(item.amount);
                                            setDescEdit(item.description);
                                            setCurrencyEdit(item.currency_id);
                                            setDateEdit(item.date);
                                            setFrequencyEdit(item.Frequency);
                                            setSubCatEdit(item.subcategory_id);
                                            setStartDateEdit(item.start_date);
                                            setEndDateEdit(item.end_date);
                                            setTransID(item.id);
                                            console.log(item);
                                        }}
                                    />
                                    <MdDelete
                                        size='1.5rem'
                                        color='red'
                                        className='point'
                                        onClick={() => {
                                            setTransID(item.id);
                                            setPopupCur(true);
                                        }}
                                    />
                                </div>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <TransPagination
                    totalPosts={data.transactions.length}
                    postsPerPage={transPerPage}
                    setCurrentPage={setCurrentPage}
                    currentPage={currentPage}/>
            </div>

            {/* ///////// DELETE Transaction ///////////////////////////////////////////////////////////////////////////////// */}
            <Popup trigger={popupCur} setTrigger={setPopupCur}>
                <h3>Are you sure you want to delete this transaction?</h3><br/>
                <button className='confirmation' onClick={() => {
                    DeleteTrans();
                    setPopupCur(false);
                    
                }}>Yes
                </button>

            </Popup>
            {/* ///// ADD Transaction Popup ////////////////////////////////////////////////////////////////////////////////////////// */}
            <Popup trigger={popupAdd} setTrigger={setPopupAdd}>
                <div className="popup_title">

                    <h3>Add Transaction</h3>
                </div>
                <form onSubmit={() => {
                    setPopupAdd(false);
                    AddTransaction()
                }}>
                    <div className="form-group">
                        <div className='input_label'>
                            <label for="title">Title:</label>
                            <input
                                type="text"
                                name="title"
                                id="title"
                                placeholder="title"
                                value={titleAdd}
                                className='admin_input'
                                onChange={(e) => {
                                    setTitleAdd(e.target.value)
                                }}
                                required

                            /></div>
                        <div className='input_label'>
                            <label for="desc">Description:</label>
                            <input
                                type="text"
                                name="desc"
                                id="desc"
                                placeholder="description"
                                className='admin_input'
                                value={descAdd}
                                onChange={(e) => {
                                    setDescAdd(e.target.value)
                                }}
                                required

                            /></div>
                        <div className='input_label'>
                            <label for="amount">Amount:</label>
                            <input
                                type="text"
                                name="amount"
                                id="amount"
                                placeholder="amount"
                                className='admin_input'
                                value={amountAdd}
                                onChange={(e) => {
                                    setAmountAdd(e.target.value)
                                }}
                                required

                            />

                            <select className='currency-list' onChange={(e) => {
                                setCurrencyAdd(e.target.value)
                            }}>
                                {currData.map(item =>
                                    <option value={item.id}>{item.Symbol}</option>
                                )}
                            </select>
                        </div>

                        <div className='radio'>
                            <div className='radio_income' onClick={() => {
                                setFrequencyAdd('fixed')
                            }}>
                                <input type={'radio'} id='fixed' value="fixed" name='frequency' defaultChecked/>
                                <label for='fixed'>Fixed</label>
                            </div>
                            <div className='radio_expense' onClick={() => {
                                setFrequencyAdd('recurent')
                            }}>
                                <input type={'radio'} id='recurent' value="recurent" name='frequency'/>
                                <label for='recurent'>Recurent</label>
                            </div>
                        </div>
                        {frequencyAdd === "fixed" ?
                            <div className='input_label'>
                                <label for="title">Date:</label>
                                <input
                                    type={"date"}
                                    name="date"
                                    id="date"
                                    placeholder="date"
                                    className='date-input'
                                    value={dateAdd}
                                    onChange={(e) => {
                                        setDateAdd(e.target.value)
                                    }}
                                    required
                                />
                            </div> :
                            <div>
                                <div className='input_label-date'>
                                    <label htmlFor="title">Start-Date:</label>
                                    <input
                                        type={"date"}
                                        name="date"
                                        id="date"
                                        placeholder="start-date"
                                        className='date-input'
                                        value={startDateAdd}
                                        onChange={(e) => {
                                            setStartDateAdd(e.target.value)
                                        }}
                                        required
                                    />
                                </div>

                                <div className='input_label-date'>
                                    <label htmlFor="title">End-Date:</label>
                                    <input
                                        type={"date"}
                                        name="date"
                                        id="date"
                                        placeholder="End-date"
                                        className='date-input'
                                        value={endDateAdd}
                                        onChange={(e) => {
                                            setEndDateAdd(e.target.value)
                                        }}
                                        required
                                    />
                                </div>
                            </div>
                        }


                        <div className='currency-list_wrapper'>
                            <label  for="title">Subcategory:</label>
                            <select className='currency-list' onChange={(e) => {
                                setSubCatAdd(e.target.value)
                            }}>
                                {subData.map(item =>
                                    <option value={item.id}>{item.name}</option>
                                )}
                            </select>
                        </div>

                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />
                    </div>
                </form>
            </Popup>
            {/* ///// EDIT Transaction Popup ////////////////////////////////////////////////////////////////////////////////////////// */}

            <Popup trigger={editPopup} setTrigger={setEditPopup}>
                <div className="popup_title">

                    <h3>Edit Transaction</h3>
                </div>
                <form onSubmit={() => {
                    setEditPopup(false);
                    EditTransaction()
                }}>
                    <div className="form-group">
                        <div className='input_label'>
                            <label for="title">Title:</label>
                            <input
                                type="text"
                                name="title"
                                id="title"
                                placeholder="title"
                                value={titleEdit}
                                className='admin_input'
                                onChange={(e) => {
                                    setTitleEdit(e.target.value)
                                }}
                                required

                            /></div>
                        <div className='input_label'>
                            <label for="desc">Description:</label>
                            <input
                                type="text"
                                name="desc"
                                id="desc"
                                placeholder="description"
                                className='admin_input'
                                value={descEdit}
                                onChange={(e) => {
                                    setDescEdit(e.target.value)
                                }}
                                required

                            /></div>
                        <div className='input_label'>
                            <label for="amount">Amount:</label>
                            <input
                                type="text"
                                name="amount"
                                id="amount"
                                placeholder="amount"
                                className='admin_input'
                                value={amountEdit}
                                onChange={(e) => {
                                    setAmountEdit(e.target.value)
                                }}
                                required

                            />
                            <select className='currency-list' value={currencyEdit} onChange={(e) => {
                                setCurrencyEdit(e.target.value)
                            }}>
                                {currData.map(item =>
                                    <option value={item.id}>{item.Symbol}</option>
                                )}
                            </select>
                        </div>
                        {frequencyEdit === "fixed" ? 
                        <div className='radio'>
                            <div className='radio_income' onClick={() => {
                                setFrequencyEdit('fixed')
                            }}>
                                <input type={'radio'} id='fixed' value="fixed" name='frequency' defaultChecked/>
                                <label for='fixed'>Fixed</label>
                            </div>
                            <div className='radio_expense' onClick={() => {
                                setFrequencyEdit('recurent')
                            }}>
                                <input type={'radio'} id='recurent' value="recurent" name='frequency'/>
                                <label for='recurent'>Recurent</label>
                            </div>
                        </div> :
                        <div className='radio'>
                        <div className='radio_income' onClick={() => {
                            setFrequencyEdit('fixed')
                        }}>
                            <input type={'radio'} id='fixed' value="fixed" name='frequency'/>
                            <label for='fixed'>Fixed</label>
                        </div>
                        <div className='radio_expense' onClick={() => {
                            setFrequencyEdit('recurent')
                        }}>
                            <input type={'radio'} id='recurent' value="recurent" name='frequency' defaultChecked/>
                            <label for='recurent'>Recurent</label>
                        </div>
                    </div> 
                        }

                        {frequencyEdit === "fixed" ?
                            <div className='input_label'>
                                <label for="title">Date:</label>
                                <input
                                    type={"date"}
                                    name="date"
                                    id="date"
                                    placeholder="date"
                                    className='date-input'
                                    value={dateEdit}
                                    onChange={(e) => {
                                        setDateEdit(e.target.value)
                                    }}
                                    required
                                />
                            </div> :
                            <div>
                                <div className='input_label-date'>
                                    <label htmlFor="title">Start-Date:</label>
                                    <input
                                        type={"date"}
                                        name="date"
                                        id="date"
                                        placeholder="date"
                                        className='date-input'
                                        value={startDateEdit}
                                        onChange={(e) => {
                                            setStartDateEdit(e.target.value)
                                        }}
                                        required
                                    />
                                </div>

                                <div className='input_label-date'>
                                    <label htmlFor="title">End-Date:</label>
                                    <input
                                        type={"date"}
                                        name="date"
                                        id="date"
                                        placeholder="date"
                                        className='date-input'
                                        value={endDateEdit}
                                        onChange={(e) => {
                                            setEndDateEdit(e.target.value)
                                        }}
                                        required
                                    />
                                </div>
                            </div>
                        }

                        <div className='currency-list_wrapper'>
                            <label for="title">Subcategory:</label>
                            <select className='currency-list' value={subCatEdit} onChange={(e) => {
                                setSubCatEdit(e.target.value)
                            }}>
                                {subData.map(item =>
                                    <option value={item.id}>{item.name}</option>
                                )}
                            </select>
                        </div>

                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />
                    </div>
                </form>
            </Popup>
        </div>
    )
}

export default Transactions;
