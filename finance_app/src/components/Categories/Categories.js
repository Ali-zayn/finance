import React, {useEffect, useState} from 'react';
import './Groups.scss';
import '../Transactions/transPagination.css'
import Sidebar from "../Sidebar/Sidebar";
import Popup from '../Popup/Popup';
import {BiEditAlt} from 'react-icons/bi';
import {MdDelete} from 'react-icons/md';
import {BsPlusSquare, BsCloudPlus} from 'react-icons/bs';
import {FaEdit, FaEye} from 'react-icons/fa';
import {HiPlus} from'react-icons/hi';
import LoadingEffect from "../LoadingEffect/LoadingEffect";
import TransPagination from "../Transactions/TransPagination";

function Groups() {

    const [data, setData] = useState(null);
    const [data1, setData1] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [loading1, setLoading1] = useState(true);
    const [id, setId] = useState(1);
    const [popup, setPopup] = useState(false);
    const [popupView, setPopupView] = useState(false);
    const [popupCur, setPopupCur] = useState(false);
    const [popupAdd, setPopupAdd] = useState(false);
    const [popupAddCat, setPopupAddCat] = useState(false);
    const [popupDelCat, setPopupDelCat] = useState(false);
    const [popupEditCat, setPopupEditCat] = useState(false);
    const [popupErr, setPopupErr] = useState(false);
    const [newName, setNewName] = useState("");
    const [newNameAdd, setNewNameAdd] = useState("");
    const [newCatName, setNewCatName] = useState("");
    const [newCatNameAdd, setNewCatNameAdd] = useState("");
    const [newCat, setNewCat] = useState(1);
    const [subcatID, setSubcatID] = useState(null);
    const [type, setType] = useState("");
    const [typeAdd, setTypeAdd] = useState("income");

    // pagination for Currencies
    const [currentSubCatPop, setCurrentSubCatPop] = useState(1);
    const [SubCatsPerPop, setSubCatsPerPop] = useState(3);
    const indexOfLastSubCat = currentSubCatPop * SubCatsPerPop;
    const indexOfFirstSubCat= indexOfLastSubCat - SubCatsPerPop;

    const token = localStorage.getItem("token");
    if (!token) {
        window.location.href = "/login";
    }

    useEffect(() => {
        FetchData(id);

    }, [id]);

    useEffect(() => {
        FetchData1();
    }, [])

/// GET All Subcategories in a Category ///////////////////////////////////////////////////////////////
    const FetchData = (id) => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/category/${id}`, {
            method: "GET",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                setData(data);
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    };
/// GET All Categories ////////////////////////////////////////////////////////////////////////
    const FetchData1 = () => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/category`, {
            method: "GET",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading1(false);
                setData1(data);
                setNewCatName(data.subCat[0].name);
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    };

    // ADD Subcategory //////////////////////////////////////////////////////////////////////////////
    const AddSubcat = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/subCategories?name=${newNameAdd}&slug=${newNameAdd}&category_id=${newCat}`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    setLoading(false);
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData1();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }

    // ADD Category //////////////////////////////////////////////////////////////////////////////
    const AddCat = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/category?name=${newCatNameAdd}&slug=${newCatNameAdd}&type=${typeAdd}`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData1();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }

// EDIT Category //////////////////////////////////////////////////////////////////////////////
    const EditCat = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/category/${id}?name=${newCatName}&slug=${newCatName}&type=${type}`, {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData1();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }


    // EDIT Subcategory //////////////////////////////////////////////////////////////////////////////
    const EditSubcat = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/subCategories/${subcatID}?name=${newName}&slug=${newName}`, {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData1();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }

    // DELETE Subcategory //////////////////////////////////////////////////////////////////////////////
    const DeleteSubcat = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/subCategories/${subcatID}`, {
            method: "DELETE",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData1();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }
    // DELETE Category //////////////////////////////////////////////////////////////////////////////
    const DeleteCat = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/category/${id}`, {
            method: "DELETE",
            headers: {"Content-Type": "application/json"},
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setLoading(false);
                FetchData1();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }


    if (loading || loading1) return (
        <div className="container">
            <Sidebar/>
            <LoadingEffect/>
        </div>
    );

    if (error) {
        setPopupErr(true);
        setError(null);
};

    return (
        <div className="container">
        <Sidebar/>
        <div className='group_wrapper'>
            <h1>Categories</h1>
            <button className='plus-butn point' onClick={() => {
                        setPopupAddCat(true)
                    }}>
            <HiPlus size='2rem' />
            </button>
            <div className="subgroup-container">
            {data1.subCat.map((item, index) =>
            <div className='cat_card'>
                <div className='cat_card_title'>
                    <hr/>
                    <h2>{item.name}</h2>
                    <hr/>
                </div>
                <h5>Type: {item.type}</h5>
                <div className='cat_card_btns'>
                    <button className='edit point' onClick={() => {
                        setPopupEditCat(true);
                        setId(item.id);
                        setNewCatName(item.name);
                        setType(item.type);
                    }}>
                        <BiEditAlt size='1.2rem' color='white'/>
                    </button>
                    <button className='trash point' onClick={() => {
                        setPopupDelCat(true);
                        setId(item.id);
                    }}>
                        <MdDelete size='1.2rem' color='white'/>
                    </button>
                    <button className='view point' onClick={() => {
                        setPopupView(true);
                        setId(item.id);
                        setNewCat(item.id);
                        setNewCatName(item.name);
                    }}>
                        <FaEye size='1.2rem' color='white'/>
                    </button>
                </div>
            </div> 
            )}
            </div>
        </div>
        
         {/* // SHOW subcategory Popup ///////////////////////////////////////////////////////////// */}
         <Popup trigger={popupView} setTrigger={setPopupView}>
            <div className="popup_title popup_sub">
                <h3>Subcategory</h3>
                <button className='plus-butn point'>
                <HiPlus size='2rem' onClick={() => {setPopupAdd(true)}}/>
                </button>
            </div>
            <table className='styled-table'>
                <thead>
                <tr>
                    <th>Subcategories</th>
                    <th>Edits</th>
                </tr>
                </thead>
                <tbody>

                {data.subCat[0].sub_catergories.slice(indexOfFirstSubCat, indexOfLastSubCat).map((item, index) => {
                    return (
                        <tr key={index}>
                            <td data-label="Subcategory">{item.name}</td>
                            <td data-label="Edits">
                                <div className='admin_edit_btns'><BiEditAlt size='1.5rem' className='point' color='blue'
                                                                            onClick={() => {
                                                                                setPopup(true);
                                                                                setSubcatID(item.id);
                                                                                setNewName(item.name)
                                                                            }}/>
                                    <MdDelete
                                        size='1.5rem' className='point' color='red' onClick={() => {
                                        setPopupCur(true);
                                        setSubcatID(item.id)
                                    }}/></div>
                            </td>

                        </tr>

                    );
                })}

                </tbody>
            </table>
             <TransPagination
                 totalPosts={data.subCat[0].sub_catergories.length}
                 postsPerPage={SubCatsPerPop}
                 setCurrentPage={setCurrentSubCatPop}
                 currentPage={setCurrentSubCatPop}/>
        </Popup>
{/* // EDIT subcategory Popup ///////////////////////////////////////////////////////////// */}
        <Popup trigger={popup} setTrigger={setPopup}>
            <div className="popup_title">
                <FaEdit size="1.5rem"/>
                <h3>Edit Subcategory</h3>
            </div>
            <form onSubmit={()=>{EditSubcat(); setPopup(false); FetchData(id)}}>
                <div className="form-group">
                <div className='input_label'>
                <label for="name">Name:</label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Name"
                        className='admin_input'
                        value={newName}
                        onChange={(e) => {
                            setNewName(e.target.value)
                        }}
                        required
                    />
                    </div>
                    <input
                        type="submit"
                        className="confirm_btn"
                        value="Confirm"
                    />
                </div>
            </form>
        </Popup>
        {/* // EDIT Category Popup ///////////////////////////////////////////////////////////// */}
        <Popup trigger={popupEditCat} setTrigger={setPopupEditCat}>
            <div className="popup_title">
                <FaEdit size="1.5rem"/>
                <h3>Edit Category</h3>
            </div>
            <form onSubmit={()=>{EditCat(); setPopupEditCat(false); FetchData1()}}>
                <div className="form-group">
                    <div className='input_label'>
                <label for="name">Name:</label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Name"
                        className='admin_input'
                        value={newCatName}
                        onChange={(e) => {
                            setNewCatName(e.target.value)
                        }}
                        required
                    />
                    </div>
                    
                        {type === 'income' ?
                        <div className='radio'>
                        <div className='radio_income' onClick={() => {setType('income')}}>
                        <input type={'radio'} id='income' value="income" name='type' checked/>
                        <label for='icome'>Income</label>
                        </div>
                        <div className='radio_expense' onClick={() => {setType('expense')}}>
                        <input type={'radio'} id='expense' value="expense" name='type' />
                        <label for='expense'>Expense</label>
                        </div>
                        </div> : 
                        <div className='radio'>
                        <div className='radio_income' onClick={() => {setType('income')}}>
                        <input type={'radio'} id='income' value="income" name='type'/>
                        <label for='icome'>Income</label>
                        </div>
                        <div className='radio_expense' onClick={() => {setType('expense')}}>
                        <input type={'radio'} id='expense' value="expense" name='type' checked/>
                        <label for='expense'>Expense</label>
                        </div>
                        </div>}
                    
                    <input
                        type="submit"
                        className="confirm_btn"
                        value="Confirm"
                    />
                </div>
            </form>
        </Popup>
        {/* // Confirmation Delete subcat Popup /////////////////////////////////////////////////////////// */}
        <Popup trigger={popupCur} setTrigger={setPopupCur}>
            <h3>Are you sure you want to delete this subcategory?</h3><br/>
            <button className='confirmation' onClick={()=>{DeleteSubcat(); setPopupCur(false); FetchData(id)}}>Yes</button>

        </Popup>
        {/* // Confirmation Delete category Popup /////////////////////////////////////////////////// */}
        <Popup trigger={popupDelCat} setTrigger={setPopupDelCat}>
            <h3>Please note that all the subcategories and transactions related to this category will be deleted as well.<br/><br/> Are
                you sure you want to delete this category?</h3><br/>
            <button className='confirmation' onClick={()=>{DeleteCat(); setPopupDelCat(false); FetchData1()}}>Yes</button>

        </Popup>
        {/* // Error handling Popup /////////////////////////////////////////////////// */}
        <Popup trigger={popupErr} setTrigger={setPopupErr}>
            <h3>You cannot enter a category/subcategory name that already exists.</h3><br/>
            <button className='confirmation' onClick={()=>{setPopupErr(false); setError(null); setPopupAdd(false); setPopupAddCat(false); setPopup(false); setPopupEditCat(false)}}>Ok</button>

        </Popup>
        {/* // ADD Subcat Popup ///////////////////////////////////////////////////////////// */}
        <Popup trigger={popupAdd} setTrigger={setPopupAdd}>
            <div className="popup_title">
                <BsCloudPlus size="1.9rem"/>
                <h3>Add Subcategory</h3>
            </div>
            <form onSubmit={()=>{AddSubcat(); setPopupAdd(false); FetchData(id)}}>
                <div className="form-group">
                <div className='input_label'>
                <label for="name">Name:</label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Name"
                        className='admin_input'
                        value={newNameAdd}
                        onChange={(e) => {
                            setNewNameAdd(e.target.value)
                        }}
                        required
                    />
                    </div>
                    
                    <input
                        type="submit"
                        className="confirm_btn"
                        value="Confirm"
                    />
                </div>
            </form>
        </Popup>
        {/* // ADD Category Popup ///////////////////////////////////////////////////////////// */}
        <Popup trigger={popupAddCat} setTrigger={setPopupAddCat}>
            <div className="popup_title">
                <BsCloudPlus size="1.9rem"/>
                <h3>Add Category</h3>
            </div>
            <form onSubmit={()=>{AddCat(); setPopupAddCat(false); FetchData1()}}>
                <div className="form-group">
                <div className='input_label'>
                <label for="name">Name:</label>
                    <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Name"
                        className='admin_input'
                        value={newCatNameAdd}
                        onChange={(e) => {
                            setNewCatNameAdd(e.target.value)
                        }}
                        required
                    /> </div>
                    
                        <div className='radio'>
                        <div className='radio_income' onClick={() => {setTypeAdd('income')}}>
                        <input type={'radio'} id='income' value="income" name='type' checked/>
                        <label for='icome'>Income</label>
                        </div>
                        <div className='radio_expense' onClick={() => {setTypeAdd('expense')}}>
                        <input type={'radio'} id='expense' value="expense" name='type' />
                        <label for='expense'>Expense</label>
                        </div>
                        </div>

                    <input
                        type="submit"
                        className="confirm_btn"
                        value="Confirm"
                    />
                </div>
            </form>
        </Popup>
    </div>)
}

export default Groups