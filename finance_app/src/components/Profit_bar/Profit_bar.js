import React from "react";
import "./Profit_bar.css"

const ProgressBar = (props) => {
    const {bgcolor, completed} = props;

    const containerStyles = {
        height: 23,
        width: '100%',
        backgroundColor: "#e0e0de",
        borderRadius: 50,
        margin: 75
    }

    const fillerStyles = {
        height: '100%',
        width: `${completed < 0 ? 0 : completed}%`,
        backgroundColor: bgcolor,
        transition: 'width 5s ease-in-out',
        borderRadius: 'inherit',
        textAlign: 'right'
    }

    const labelStyles = {
        padding: 30,
        color: 'white',
        fontWeight: 'bold'
    }


    return (
        <div className="barShape">

            <div style={containerStyles}>
                <div style={fillerStyles}>
                    <span style={labelStyles}>{`${completed}%`}</span>
                </div>
            </div>
        </div>
    );
};

export default ProgressBar;