import React from 'react'
import './Loading.css';

function LoadingEffect() {
  return (
    <div className='loading_wrapper'>
        <div className='loading_circle'>
            <p className='dollar_rotating'>$</p>
        </div>
    </div>
  )
}

export default LoadingEffect