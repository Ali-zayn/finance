import React, {useState, useEffect} from "react";
import './Login.css';
import axios from 'axios';
import {useNavigate} from 'react-router-dom';

function Login() {
    const navigate = useNavigate();
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [data, setData] = useState(null);
    const [auth, setAuth] = useState(null);
    const [formData, setFormData] = useState({
        username: "",
        password: "",
    })
    const [error, setError] = useState(null);

    const onSubmit = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/adminLogin?username=${username}&password=${password}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },

        })
            .then((reponse) => {
                if (reponse.ok) {
                    return reponse.json();
                }
                throw reponse;
            })
            .then((data) => {
                setData(data);
                //
                // if(data.length === 0){
                //     return alert("incorrect username and password");
                // }
                if (data.status.length === 0) {
                    return alert("incorrect username and password");
                }
                 else if (data.status[0].isActive === true){

                    localStorage.setItem("token", data.token);
                    localStorage.setItem("role", data.status[0].name);
                    localStorage.setItem("id", data.status[0].id);
                    navigate("/home");
                } else if( data.status[0].isActive === false){
                    return alert("inactive Admin");
                }

            }, );
    };



    if(error) return "Error...";


    return (
        <div className="wrapper fadeInDown">
            <div id="formContent">

                <h2 className="active"> Sign In </h2>


                <div className="fadeIn first">
                    <img src="/images/login_image.png" id="icon" alt="User Icon"/>
                </div>


                <form onSubmit={onSubmit}>
                    <input type="text" id="login" className="fadeIn second" name="login" placeholder="username" required onChange={(e)=>{setUsername(e.target.value)}}/>
                    <input type="password" id="password" className="fadeIn third" name="login" placeholder="password" required onChange={(e)=>{setPassword(e.target.value)}}/>
                    <input type="submit" className="fadeIn fourth login_btn" value="Log In"/>
                </form>
            </div>

        </div>

    )
}

export default Login;