import React, { useEffect, useState } from 'react';
import './Goals.css';
import Sidebar from '../Sidebar/Sidebar';
import { BiEditAlt } from 'react-icons/bi';
import { BsCloudPlus } from 'react-icons/bs';
import Popup from '../Popup/Popup';
import { FiPlusCircle } from "react-icons/fi";
import { FaEdit } from "react-icons/fa";
import LoadingEffect from "../LoadingEffect/LoadingEffect";
import Goal_card from './Goal_card';
import axios from 'axios';
import { HiPlus } from "react-icons/hi";


function Goals() {
    const token = localStorage.getItem("token");
    if (!token) {
        window.location.href = "/login";
    }
    const [popup, setPopup] = useState(false);
    const [popupErr, setPopupErr] = useState(false);
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [loading1, setLoading1] = useState(true);
    const [goalID, setGoalID] = useState(null);
    const [amount, setAmount] = useState(null);
    const [year, setYear] = useState(null);
    const [popup2, setPopup2] = useState(false);
    const [amount1, setAmount1] = useState(null);
    const [year1, setYear1] = useState(null);
    const [trans, setTrans] = useState(null);


    useEffect(() => {
        FetchData();
        FetchTrans();
    }, []);
    // Get All Goals ///////////////////////////////////////////////////////////////////////////////
    const FetchData = () => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/profit_goals`, {
            method: "GET",
            headers: { "Content-Type": "application/json" },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                setData(data);
                setLoading(false);
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    };
    // Get All Transactions ///////////////////////////////////////////////////////////////////////////////
    const FetchTrans = async () => {
        setLoading1(true);
        await axios.get(`${process.env.REACT_APP_BACKEND_URL}api/transaction`, {
            headers: { "Content-Type": "multipart/form-data" },
        })
            .then((res) => {
                setTrans(res.data);
                setLoading1(false);

            })

            .catch((error) => {
                setError(error);
                console.error(error.message)
            });
    };

    // edit Goals ///////////////////////////////////////////////////////////////////////////////
    const EditGoal = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/profit_goals/${goalID}?year=${year}&amount=${amount}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                FetchData();
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });

    }

    // add Goals ///////////////////////////////////////////////////////////////////////////////
    const AddGoal = () => {
        setLoading(true);
        fetch(`${process.env.REACT_APP_BACKEND_URL}api/profit_goals?amount=${amount1}&year=${year1}`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                FetchData();
                setAmount1(null);
                setYear1(null);
            })
            .catch((error) => {
                console.error(error.message);
                setError(error);
            });
    }
/////////if adding exciting goal year//////
    if (error) {
        setPopupErr(true);
        setError(null);
    };

    if (loading || loading1) return (
        <div><Sidebar /><LoadingEffect /></div>

    );


    return (
        <div className='container'>
            <Sidebar />
            <div className='goal_wrapper'>
                <h1>Profit Goals</h1>
                <button className='plus-butn point' onClick={() => {
                    setPopup2(true)
                }}>
                    <HiPlus size='2rem' />
                </button>
                <div className='goals_display'>
                    {data.map(item =>
                        <Goal_card id={item.id} data={trans} total={item.amount} year={item.year} setPopup={setPopup} setAmount={setAmount} setYear={setYear} setGoalID={setGoalID} />
                    )}
                </div>
            </div>
            {/* // Error handling Popup /////////////////////////////////////////////////// */}
            <Popup trigger={popupErr} setTrigger={setPopupErr}>
                <h3>You cannot enter a year that already has a goal.</h3><br />
                <button className='confirmation' onClick={() => { setPopupErr(false); setError(null); setPopup2(false); setPopup(false); }}>Ok</button>

            </Popup>
            {/* /// Edit Goal Popup /////////////////////////////////////////////////////////////////// */}
            <Popup trigger={popup} setTrigger={setPopup}>

                <div className="popup_title">
                    <FaEdit size='1.5rem' />
                    <h3>Edit Goal</h3> 
                </div>
                <form onSubmit={() => { EditGoal(); setPopup(false); }}>
                    <div className="form-group">
                        <h4>{year}</h4>
                        <div className='input_label'>
                            <label for="name">Amount:</label>
                            <input
                                type="text"
                                name="name"
                                id="name"
                                placeholder="Amount"
                                value={amount}
                                className='admin_input'
                                onChange={(e) => {
                                    setAmount(e.target.value)
                                }}
                                required

                            />
                        </div>


                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />


                    </div>
                </form>
            </Popup>
            {/* /// Add Goal Popup ///////////////////////////////////////////////////////// */}
            <Popup trigger={popup2} setTrigger={setPopup2}>

                <div className="popup_title">
                    <BsCloudPlus size="1.9rem" />
                    <h3>Add Profit Goal</h3>
                </div>
                <form onSubmit={() => { AddGoal(); setPopup2(false); }}>
                    <div className="form-group">
                        <div className='input_label'>
                            <label for="name">Amount:</label>
                            <input
                                type="text"
                                name="amount"
                                id="amount"
                                placeholder="amount"
                                className='admin_input'
                                value={amount1}
                                onChange={(e) => {
                                    setAmount1(e.target.value)
                                }}
                            />
                        </div>
                        <div className='input_label'>
                            <label for="name">Year:</label>
                            <input
                                type="text"
                                name="year"
                                id="year"
                                placeholder="year"
                                className='admin_input'
                                value={year1}
                                onChange={(e) => {
                                    setYear1(e.target.value)
                                }}
                            />
                        </div>


                        <input
                            type="submit"
                            className="confirm_btn"
                            value="Confirm"
                        />
                    </div>
                </form>
            </Popup>

        </div>

    )
}

export default Goals