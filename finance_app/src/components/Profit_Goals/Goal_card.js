import React, { useEffect, useState } from 'react'
import './Goal_card.css'
import { BiEditAlt } from 'react-icons/bi'
import LoadingEffect from '../LoadingEffect/LoadingEffect';
import axios from 'axios';

function Goal_card(props) {
  const [percent, setPercent] = useState(null);
  const [amount, setAmount] = useState(null);
  const [trans, setTrans] = useState([]);
  const [color, setColor] = useState(null);
  const [color1, setColor1] = useState("#f0f0f0");
  const [color2, setColor2] = useState("#fefefe");

  //////////////////////////////////////////////////////////////////
  const Calcpercent = () => {
    let perc = 0;
    perc = Math.trunc((amount / props.total) * 100);
    setPercent(perc);
    if(percent <= 25) {
      setColor("red");
    } else if(percent <= 50) {
      setColor("orange");
    } else if(percent <= 75) {
      setColor("green")
    } else if(percent < 100) {
      setColor("rgb(84, 150, 255)")
    } else {
      setColor("rgb(255, 242, 0)");
      setColor1("rgb(255, 252, 200)");
      setColor2("rgb(255, 242, 0)");
    }
  }
  //////////////////////////////////////////////////////////////////
  const filterTrans = () => {
    console.log("bruh");
    let arr = [];
    props.data.transactions.filter( item => {
      if (item.date.includes(props.year)) {
      arr.push(item);  
      }
  })
  setTrans(arr);
  
  let amountTrans = 0;
  let totalAmount = 0;
  
  trans.forEach(item => {
    if (item.subcategory.category.type === 'expense') {
      amountTrans = (item.amount / item.currency.rate) * -1;
      
    } else {
      amountTrans = item.amount / item.currency.rate;
    }
    totalAmount += amountTrans;
  }); 
  setAmount(totalAmount.toFixed(2));
  Calcpercent();
}

////////////////////////////////////////////////////////////////////
useEffect(() => {
  filterTrans(); 
}, [amount, percent]); 
 
  return (
    <div className='gcard' style={{ "--clr1": color1}}>
      <BiEditAlt size='1.3rem' color='black' className='edit_goal' onClick={() => {
        props.setPopup(true);
        props.setAmount(props.total);
        props.setYear(props.year);
        props.setGoalID(props.id);

      }} />
      <div className='percent' style={{ "--num": percent, "--clr": color, "--clr2": color2}}>
        <div className='goal_year'>
          <p>{props.year}</p>
        </div>
        <div className='dot'></div>
        <svg>
          <circle cx='70' cy='70' r='70'></circle>
          <circle cx='70' cy='70' r='70'></circle>
        </svg>
        <div className='number'>
          <h2>{percent}<span>%</span></h2>
          <p>{amount}$</p>
        </div>
        <div className='goal_total'><p>{props.total}$</p></div>

      </div>
    </div>
  )
}

export default Goal_card