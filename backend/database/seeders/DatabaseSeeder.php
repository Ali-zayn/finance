<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\category;
use App\Models\currency;
use App\Models\profit_goals;
use App\Models\subcategories;
use App\Models\admin;
use App\Models\roles;

use App\Models\transactions;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roles = [[
            "name" => "superAdmin",
            "slug" => "superAdmin"
        ], [
            "name" => "admin",
            "slug" => "admin"
        ]];
        foreach ($roles as $role) {
            roles::create($role);
        }
        $admins = [[
            'username' => 'Ali Ashaal',
            'password' => 'Ali12345',
            'f_name' => 'Ali',
            'l_name' => 'Ashaal',
            'email' => 'Aliashaal@gmail.com',
            'phoneNumber' => '0096171970947',
            'picture' => 'Ali_Ashaal.jpg',
            'isActive' => 1,
            'role_id' => 1,
            'slug' => 'Ali Ashaal'

        ], [
            'username' => 'Ali Karkalla',
            'password' => 'Ali12345',
            'f_name' => 'Ali',
            'l_name' => 'Karkalla',
            'email' => 'AliKarkalla@gmail.com',
            'phoneNumber' => '009613122606',
            'picture' => 'ali-k.jpg',
            'isActive' => 1,
            'role_id' => 2,
            'slug' => 'Ali Karkalla'

        ],[
                'username' => 'Peter Abboud',
                'password' => 'Peter12345',
                'f_name' => 'Peter',
                'l_name' => 'Abboud',
                'email' => 'PeterAbboud@gmail.com',
                'phoneNumber' => '0096181762639',
                'picture' => 'Peter.jpg',
                'isActive' => 1,
                'role_id' => 1,
                'slug' => 'Peter Abboud'

            ],[
            'username' => 'Mazen Zeineddine',
            'password' => 'Mazen12345',
            'f_name' => 'Mazen',
            'l_name' => 'Zeineddine',
            'email' => 'MazenZeineddine@gmail.com',
            'phoneNumber' => '0096181775343',
            'picture' => 'Mazen.jpeg',
            'isActive' => 1,
            'role_id' => 2,
            'slug' => 'Mazen Zeineddine'

        ],[
            'username' => 'Ali Shamas',
            'password' => 'Ali12345',
            'f_name' => 'Ali',
            'l_name' => 'Shamas',
            'email' => 'AliShamas@gmail.com',
            'phoneNumber' => '0096171422423',
            'picture' => 'Ali-Shamas.jpeg',
            'isActive' => 0,
            'role_id' => 1,
            'slug' => 'Ali Shamas'

        ],[
            'username' => 'Ali Masri',
            'password' => 'Ali12345',
            'f_name' => 'Ali',
            'l_name' => 'Masri',
            'email' => 'AliMasri@gmail.com',
            'phoneNumber' => '0096171861634',
            'picture' => 'ali-massry.jpg',
            'isActive' => 0,
            'role_id' => 2,
            'slug' => 'Ali Masri'

        ]];
        foreach ($admins as $admin) {
            admin::create($admin);
        }
        $categories = [[
            'name' => 'Bills',
            'slug' => 'Bills',
            'type' => 'expense'

        ],[
            'name' => 'Rentals',
            'slug' => 'Rentals',
            'type' => 'expense'

        ],[
                'name' => 'Sales',
                'slug' => 'Sales',
                'type' => 'income'

        ],[
                'name' => 'Insurance',
                'slug' => 'Insurance',
                'type' => 'expense'

        ],[
            'name' => 'Refreshments',
            'slug' => 'Refreshments',
            'type' => 'expense'

        ],[
            'name' => 'Royalties',
            'slug' => 'Royalties',
            'type' => 'income'

        ],[
            'name' => 'Capital Gains',
            'slug' => 'Capital Gains',
            'type' => 'income'

        ]];
        foreach ($categories as $category) {
            category::create($category);
        }
        $currencies = [[
            'name' => 'American Dollar',
            'rate' => 1,
            'ISO' => 'USD',
            'Symbol' => '$',
            'slug' => 'American Dollar'

        ],[
            'name' => 'Euro',
            'rate' => 1.2,
            'ISO' => 'EUR',
            'Symbol' => '€',
            'slug' => 'Euro'

        ],[
            'name' => 'Lebanese Lira',
            'rate' => 32000,
            'ISO' => 'LBP',
            'Symbol' => 'L.L.',
            'slug' => 'Lebanese Lira'
        ]];
        foreach ($currencies as $currency) {
            currency::create($currency);
        }
        $profit_goals = [[
            'amount' => 4500,
            'year' => 2018

        ],[
            'amount' => 7500,
            'year' => 2019

        ],[
            'amount' => 16000,
            'year' => 2020


        ],[
            'amount' => 25000,
            'year' => 2021

        ]];
        foreach ($profit_goals as $profit_goal) {
            profit_goals::create($profit_goal);
        }
        $subcategories = [[
            'name' => 'Phone',
            'slug' => 'Phone',
            'category_id' => 1

        ],[
            'name' => 'Water',
            'slug' => 'Water',
            'category_id' => 1

        ],[
            'name' => 'Electricity',
            'slug' => 'Electricity',
            'category_id' => 1

        ],[
            'name' => 'Internet',
            'slug' => 'Internet',
            'category_id' => 1

        ],[
            'name' => 'Location',
            'slug' => 'Location',
            'category_id' => 2

        ],[
            'name' => 'Transportation',
            'slug' => 'Transportation',
            'category_id' => 2

        ],[
            'name' => 'Parking',
            'slug' => 'Parking',
            'category_id' => 2

        ],[
            'name' => 'B2B',
            'slug' => 'B2B',
            'category_id' => 3

        ],[
            'name' => 'SaaS',
            'slug' => 'SaaS',
            'category_id' => 3

        ],[
            'name' => 'Direct',
            'slug' => 'Direct',
            'category_id' => 3

        ],[
            'name' => 'B2C',
            'slug' => 'B2C',
            'category_id' => 3

        ],[
            'name' => 'Health',
            'slug' => 'Health',
            'category_id' => 4

        ],[
            'name' => 'Workers compensation',
            'slug' => 'Workers compensation',
            'category_id' => 4

        ],[
            'name' => 'Vehicle',
            'slug' => 'Vehicle',
            'category_id' => 4

        ],[
            'name' => 'Drinks',
            'slug' => 'Drinks',
            'category_id' => 5

        ],[
            'name' => 'Food',
            'slug' => 'Food',
            'category_id' => 5

        ],[
            'name' => 'Patent',
            'slug' => 'Patent',
            'category_id' => 6

        ],[
            'name' => 'Franchise',
            'slug' => 'Franchise',
            'category_id' => 6

        ],[
            'name' => 'Short-Term',
            'slug' => 'Short-Term',
            'category_id' => 7

        ],[
            'name' => 'Long-Term',
            'slug' => 'Long-Term',
            'category_id' => 7

        ]];
        foreach ($subcategories as $subcategory) {
            subcategories::create($subcategory);
        }
        $transacations = [[
            'title' => 'Delivery van',
            'description' => 'expand business with deliveries',
            'amount' => 2000,
            'date' => "2018-07-05",
            'Frequency' => 'fixed',
            'slug' => 'Delivery van',
            'currency_id' => 1,
            'subcategory_id' => 6

        ],[
            'title' => 'Water stations',
            'description' => 'water stations',
            'amount' => 2300000,
            'date' => "2018-07-07",
            'start_date' => "2018-07-07",
            'end_date' => "2018-08-07",
            'Frequency' => 'recurrent',
            'slug' => 'Water stations',
            'currency_id' => 3,
            'subcategory_id' => 2

        ],[
            'title' => 'Internet router',
            'description' => 'more routers for the new floor',
            'amount' => 100,
            'date' => "2018-08-15",
            'Frequency' => 'fixed',
            'slug' => 'Internet router',
            'currency_id' => 2,
            'subcategory_id' => 4

        ],[
            'title' => 'total sales B2B',
            'description' => 'Sales to customers',
            'amount' => 2000,
            'date' => "2018-09-12",
            'start_date' => "2018-09-12",
            'end_date' => "2018-10-12",
            'Frequency' => 'recurrent',
            'slug' => 'total sales B2B',
            'currency_id' => 1,
            'subcategory_id' => 8

        ],[
            'title' => 'total sales Direct',
            'description' => 'Sales Products',
            'amount' => 3000000,
            'date' => "2018-07-08",
            'Frequency' => 'fixed',
            'slug' => 'total sales Direct',
            'currency_id' => 3,
            'subcategory_id' => 10

        ],[
            'title' => 'Short-Term sales',
            'description' => 'Short-Term sales to customers',
            'amount' => 1000,
            'date' => "2018-07-10",
            'start_date' => "2018-07-10",
            'end_date' => "2018-09-10",
            'Frequency' => 'recurrent',
            'slug' => 'Short-Term sales',
            'currency_id' => 2,
            'subcategory_id' => 19

        ],[
            'title' => 'Phone fixing',
            'description' => 'fix available phones ',
            'amount' => 100,
            'date' => "2019-04-25",
            'Frequency' => 'fixed',
            'slug' => 'Phone fixing',
            'currency_id' => 1,
            'subcategory_id' => 1

        ],[
            'title' => 'Electricity Bill',
            'description' => 'Electricity Bill for the 2019 year',
            'amount' => 1000,
            'date' => "2019-04-22",
            'start_date' => "2019-04-22",
            'end_date' => "2019-05-13",
            'Frequency' => 'recurrent',
            'slug' => 'Electricity Bill',
            'currency_id' => 2,
            'subcategory_id' => 3

        ],[
            'title' => 'rent 2019',
            'description' => 'Location rent building',
            'amount' => 1000000,
            'date' => "2019-06-23",
            'Frequency' => 'fixed',
            'slug' => 'rent 2019',
            'currency_id' => 3,
            'subcategory_id' => 5

        ],[
            'title' => 'employees health',
            'description' => 'check employees health',
            'amount' => 1500,
            'date' => "2019-07-02",
            'start_date' => "2019-07-02",
            'end_date' => "2019-07-13",
            'Frequency' => 'recurrent',
            'slug' => 'employees health',
            'currency_id' => 2,
            'subcategory_id' => 12

        ],[
            'title' => 'SaaS Sales Mahmoud',
            'description' => 'Sales in cash and credit to customers',
            'amount' => 3000,
            'date' => "2019-04-05",
            'Frequency' => 'fixed',
            'slug' => 'SaaS Sales mahmoud',
            'currency_id' => 1,
            'subcategory_id' => 9

        ],[
            'title' => 'B2C from Joe',
            'description' => 'Payment of cash from a costumer from a sent invoice',
            'amount' => 4500,
            'date' => "2019-02-13",
            'start_date' => "2019-02-13",
            'end_date' => "2019-04-13",
            'Frequency' => 'recurrent',
            'slug' => 'B2C from Joe',
            'currency_id' => 2,
            'subcategory_id' => 11
        ],[
            'title' => 'Long-Term sales for Sara',
            'description' => 'Cash Sale of Goods',
            'amount' => 1400,
            'start_date' => "2019-08-25",
            'date' => "2019-08-25",
            'end_date' => "2019-09-15",
            'Frequency' => 'recurrent',
            'slug' => 'sales',
            'currency_id' => 1,
            'subcategory_id' => 20

        ],[
            'title' => 'Internal Transaction',
            'description' => 'Buying insurance from an insurer',
            'amount' => 1300000,
            'date' => "2019-06-12",
            'Frequency' => 'fixed',
            'slug' => 'Internal transactions',
            'currency_id' => 3,
            'subcategory_id' => 20

        ],[
        'title' => 'Employee transportation',
            'description' => 'Employee transportation funds',
            'amount' => 2000,
            'date' => "2020-07-05",
            'Frequency' => 'fixed',
            'slug' => 'Employee transportation',
            'currency_id' => 1,
            'subcategory_id' => 6

        ],[
        'title' => 'Water',
        'description' => 'water fixed',
        'amount' => 23000000,
        'date' => "2020-07-07",
        'start_date' => "2020-07-07",
        'end_date' => "2020-08-07",
        'Frequency' => 'recurrent',
        'slug' => 'Water2020',
        'currency_id' => 3,
        'subcategory_id' => 2

    ],[
        'title' => 'Internet fund',
        'description' => 'Internet payment',
        'amount' => 1000,
        'date' => "2020-08-15",
        'Frequency' => 'fixed',
        'slug' => 'Internet 2020',
        'currency_id' => 2,
        'subcategory_id' => 4

    ],[
        'title' => 'total sales B2B Naji',
        'description' => 'Sales to customers',
        'amount' => 6000,
        'date' => "2020-09-12",
        'start_date' => "2020-09-12",
        'end_date' => "2020-10-12",
        'Frequency' => 'recurrent',
        'slug' => 'total sales B2B Naji',
        'currency_id' => 1,
        'subcategory_id' => 8

    ],[
        'title' => 'total sales Direct Amer',
        'description' => 'Sales Products Amer ',
        'amount' => 20000000,
        'date' => "2020-07-08",
        'Frequency' => 'fixed',
        'slug' => 'total sales Direct Amer',
        'currency_id' => 3,
        'subcategory_id' => 10

    ],[
        'title' => 'Short-Term sales Hani',
        'description' => 'Short-Term sales to customers Hani',
        'amount' => 4000,
        'date' => "2020-07-10",
        'start_date' => "2020-07-10",
        'end_date' => "2020-09-10",
        'Frequency' => 'recurrent',
        'slug' => 'Short-Term sales Hani',
        'currency_id' => 2,
        'subcategory_id' => 19

    ],[
        'title' => 'Phone calls',
        'description' => 'calling customers ',
        'amount' => 100,
        'date' => "2020-04-25",
        'Frequency' => 'fixed',
        'slug' => 'Phone calls 2020',
        'currency_id' => 1,
        'subcategory_id' => 1

    ],[
        'title' => 'Electricity mentor',
        'description' => 'Electricity mentor for the 2020 year',
        'amount' => 10000,
        'date' => "2020-04-22",
        'start_date' => "2020-04-22",
        'end_date' => "2020-05-13",
        'Frequency' => 'recurrent',
        'slug' => 'Electricity mentor 2020',
        'currency_id' => 2,
        'subcategory_id' => 3

    ],[
        'title' => 'rent 2020',
        'description' => 'Location rent building',
        'amount' => 10000000,
        'date' => "2020-06-23",
        'Frequency' => 'fixed',
        'slug' => 'rent 2020',
        'currency_id' => 3,
        'subcategory_id' => 5

    ],[
        'title' => 'employees medicine',
        'description' => 'check employees medicine',
        'amount' => 5000,
        'date' => "2020-07-02",
        'start_date' => "2020-07-02",
        'end_date' => "2020-07-13",
        'Frequency' => 'recurrent',
        'slug' => 'employees medicine 2020',
        'currency_id' => 2,
        'subcategory_id' => 12

    ],[
        'title' => 'SaaS Sales Ahmad',
        'description' => 'Sales in cash and credit to Ahmad',
        'amount' => 30000,
        'date' => "2020-04-05",
        'Frequency' => 'fixed',
        'slug' => 'SaaS Sales Ahmad',
        'currency_id' => 1,
        'subcategory_id' => 9

    ],[
        'title' => 'B2C from Tati',
        'description' => 'Payment of cash from a costumer from a sent invoice Tati',
        'amount' => 4500,
        'date' => "2020-02-13",
        'start_date' => "2020-02-13",
        'end_date' => "2020-04-13",
        'Frequency' => 'recurrent',
        'slug' => 'B2C from Tati',
        'currency_id' => 2,
        'subcategory_id' => 11
    ],[
        'title' => ' sales for Tony',
        'description' => 'Cash Sale of Goods',
        'amount' => 14000,
        'date' => "2020-08-25",
        'Frequency' => 'fixed',
        'slug' => 'sales Tony',
        'currency_id' => 1,
        'subcategory_id' => 8

    ],[
        'title' => 'Internal Transaction Peter',
        'description' => 'Buying insurance from an insurer Peter',
        'amount' => 2500000,
        'date' => "2020-06-12",
        'Frequency' => 'fixed',
        'slug' => 'Internal transactions Peter',
        'currency_id' => 3,
        'subcategory_id' => 20

    ]];
        foreach ($transacations as $transacation) {
            transactions::create($transacation);
        }



    }
}
