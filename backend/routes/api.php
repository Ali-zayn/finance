<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::post('/createGroups/',[Controllers\groupController::class, 'createGroup']);
//Route::get('/getGroups/{name}',[Controllers\groupController::class, 'getGroup']);
//Route::put('/editGroups/{name}',[Controllers\groupController::class, 'editGroup']);
//Route::delete('/deleteGroups/{name}',[Controllers\groupController::class, 'deleteGroup']);
//Route::post('/createSubGroups/',[Controllers\subGroupsController::class, 'createSubGroup']);
Route::resource('/category', Controllers\categoriesController::class);
Route::resource('/subCategories', Controllers\subcatergoriesController::class);
Route::resource('/roles', Controllers\rolesController::class);
Route::resource('/admins', Controllers\adminController::class);
Route::resource('/adminLogin', Controllers\AdminloginController::class);
Route::resource('/currency', Controllers\currencyController::class);
Route::resource('/profit_goals', Controllers\profit_goalsController::class);
Route::resource('/transaction', Controllers\transactionsController::class);
Route::get('/transactions/{date}', [Controllers\transactionsController::class, 'getTransaction']);