<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
    ];
    public  function subCatergories() {
        return $this->hasMany(subcategories::class, 'category_id');

    }
}
