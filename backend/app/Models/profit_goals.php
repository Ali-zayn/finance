<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profit_goals extends Model
{
    use HasFactory;

    protected $fillable = [
        'year',
        'amount',

    ];
}
