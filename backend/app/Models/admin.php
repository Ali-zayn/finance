<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    use HasFactory;

    protected $fillable = [
        'username',
        'password',
        'f_name',
        'l_name',
        'email',
        'phoneNumber',
        'picture',
        'isActive',
        'role_id',
    ];
    public  function admin() {
        return $this->belongsTo(roles::class,'role_id');

    }

}
