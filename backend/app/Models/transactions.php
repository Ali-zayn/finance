<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\category;

class transactions extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'amount',
        'date',
        'start_date',
        'end_date',
        'Frequency',
        'currency_id',
        'subcategory_id',
    ];

    public function currency()
    {
        return $this->belongsTo(currency::class, 'currency_id');

    }

    public function subcategory()
    {
        return $this->belongsTo(subcategories::class, 'subcategory_id');

    }

//    public function categories1()
//    {
//        return $this->belongsTo(subcategories::class, 'subcategory_id');
//
//    }
}
