<?php

namespace App\Http\Controllers;

use App\Models\profit_goals;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;


class profit_goalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $profitGoals = new profit_goals();
//        return $profitGoals->get();

        $profitGoals = DB::table('profit_goals')
            ->orderBy('year', 'desc');
            return $profitGoals-> get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profitGoal = new profit_goals();
        $amount = $request->input('amount');
        $year = $request->input('year');
        $profitGoal->amount = $amount;
        $profitGoal->year = $year;
        $profitGoal->save();
        return response()->json([
            "profitGoal was created"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profitGoal = profit_goals::where('id', $id)->get();

        return response()->json([
            $profitGoal
        ]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profitGoal = profit_goals::where('id', $id);
        $inputsProfitGoals = $request->except(['_method', 'token']);
        $profitGoal->update($inputsProfitGoals);
        return response()->json([
            "ProfitGolas has been updated"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profitGoal = profit_goals::where('id', $id);
        $profitGoal->delete();
        return response()->json([
            "profitGoal has been deleted"
        ]);
    }
}
