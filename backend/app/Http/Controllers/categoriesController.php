<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;


class categoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with("subCatergories")
            ->get();
        return response()->json([
                "subCat" => $categories
            ]
        );
//        $getAll = new category();
//        return $getAll->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new category();
        $name = $request -> input('name');
        $type = $request -> input('type');
        $slug = $request -> input('slug');
        $category -> name = $name;
        $category -> type = $type;
        $category -> slug = $slug;
        $category -> save();
        return response()->json([
            "Category was created"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = category::where('id', $id)-> with("subCatergories")->get();

        return response()->json([
            "subCat" => $category
        ]);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = category::where('id', $id);
        $inputsCategory = $request -> except(['_method', 'token']);
        $category -> update($inputsCategory);
        return response()->json([
            "category has been updated"        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = category::where('id', $id);
        $category -> delete();
        return response()->json([
            "category has been deleted"
        ]);
    }
}
