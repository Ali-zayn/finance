<?php

namespace App\Http\Controllers;

use App\Models\admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = DB::table('admins')
            ->orderBy('isActive', 'desc')
            ->orderBy('role_id')
            ->get();
        return response()->json([
                "admins" => $admins
            ]
        );
//        $getAll = new admin();
//
//        return $getAll->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Admin = new admin();
        if ($request->picture) {
            $getPicture = $request->picture;
            $pictureName = $getPicture->getClientOriginalName();
            $picturePath = public_path() . '/pictures';
            $getPicture->move($picturePath, $pictureName);
            $Admin->picture = $pictureName;
        }
        $username = $request->input('username');
        $password = $request->input('password');
        $f_name = $request->input('f_name');
        $l_name = $request->input('l_name');
        $email = $request->input('email');
        $phoneNumber = $request->input('phoneNumber');

        $isActive = $request->input('isActive');
        $role_id = $request->input('role_id');
        $slug = $request->input('slug');
        $Admin->username = $username;
        $Admin->password = $password;
        $Admin->f_name = $f_name;
        $Admin->l_name = $l_name;
        $Admin->email = $email;
        $Admin->phoneNumber = $phoneNumber;

        $Admin->isActive = $isActive;
        $Admin->slug = $slug;
        $Admin->role_id = $role_id;
        $Admin->save();

        return response()->json([
            "Admin was created"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Admin = admin::where('id', $id)->with("admin")->get();
        return response()->json([
            $Admin

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Admin = admin::findOrFail($id);

        if ($request->picture) {
            $picturePath = public_path() . '/pictures/';
            //code for remove old file
            if ($Admin->picture = null && $Admin->picture = "") {
                $file_old = $picturePath . $Admin->picture;
                unlink($file_old);
            }
            //upload new file
            $file = $request->picture;
            $filename = $file->getClientOriginalName();
            $file->move($picturePath, $filename);
            //for update in table
            $Admin->picture = $filename;
        }

        $inputsAdmin = $request->except(['_method', 'token', 'picture']);
        $Admin->update($inputsAdmin);
        return response()->json([
            "Admin updated"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Admin = admin::where('id', $id);
        $Admin->delete();
        return response()->json([
            "Admin has been deleted"
        ]);
    }

}
