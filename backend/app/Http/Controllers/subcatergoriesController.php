<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\subcategories;

class subcatergoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getAll = new subcategories();
        return $getAll->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subCategory = new subcategories();
        $name = $request -> input('name');
        $category_id = $request -> input('category_id');
        $slug = $request -> input('slug');
        $subCategory -> name = $name;
        $subCategory -> slug = $slug;
        $subCategory -> category_id = $category_id;
        $subCategory -> save();
        return response()->json([
            "subCategory was created"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subCategory = subcategories::where('id', $id) ->get();
        return response()->json([
            $subCategory
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subCategory = subcategories::where('id', $id);
        $inputsSubCategory = $request -> except(['_method', 'token']);
        $subCategory -> update($inputsSubCategory);
        return response()->json([
            "subCategory has been updated"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subCategory = subcategories::where('id', $id);
        $subCategory -> delete();
        return response()->json([
            "subCategory has been deleted"
        ]);
    }
}
