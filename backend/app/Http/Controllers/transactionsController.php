<?php

namespace App\Http\Controllers;

use App\Models\transactions;
use Illuminate\Http\Request;

class transactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = transactions::with("subcategory.category", "currency")
            ->orderBy('date', 'Desc')

            ->get();
        return response()->json([
                "transactions" => $transactions
            ]
        );
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTransaction($date)
    {
        $transactions = transactions::whereYear('date', $date)->with("subcategory.category", "currency")->get();
        
        return response()->json([
                "transactions" => $transactions
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new transactions();
        $title = $request -> input('title');
        $description = $request -> input('description');
        $amount = $request -> input('amount');
        $date = $request -> input('date');
        $start_date = $request -> input('start_date');
        $end_date = $request -> input('end_date');
        $Frequency = $request -> input('Frequency');
        $currency_id = $request -> input('currency_id');
        $subcategory_id = $request -> input('subcategory_id');
        $slug = $request -> input('slug');
        $transaction -> title = $title;
        $transaction -> description = $description;
        $transaction -> amount = $amount;
        $transaction -> date = $date;
        $transaction -> start_date = $start_date;
        $transaction -> end_date = $end_date;
        $transaction -> Frequency = $Frequency;
        $transaction -> currency_id = $currency_id;
        $transaction -> subcategory_id = $subcategory_id;
        $transaction -> slug = $slug;
        $transaction -> save();
        return response()->json([
            "transaction was created"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = transactions::where('id', $id)->get();

        return response()->json([
            "transactions" => $transaction
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = transactions::where('id', $id);
        $inputsTransaction = $request -> except(['_method', 'token']);
        $transaction -> update($inputsTransaction);
        return response()->json([
            "transaction has been updated"
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = transactions::where('id', $id);
        $transaction -> delete();
        return response()->json([
            "transaction has been deleted"
        ]);
    }

    // get all transaction where date include year


}
