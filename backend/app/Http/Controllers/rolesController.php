<?php

namespace App\Http\Controllers;

use App\Models\roles;
use Illuminate\Http\Request;

class rolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getAll = new roles();
        return $getAll->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Role = new roles();
        $name = $request -> input('name');
        $slug = $request -> input('slug');
        $Role -> name = $name;
        $Role -> slug = $slug;
        $Role -> save();
        return response()->json([
            "Role was created"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Role = roles::where('id', $id) ->get();
        return response()->json([
            $Role
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Role = roles::where('id', $id);
        $inputsRole = $request -> except(['_method', 'token']);
        $Role -> update($inputsRole);
        return response()->json([
            "Role has been updated"
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Role = roles::where('id', $id);
        $Role -> delete();
        return response()->json([
            "role has been deleted"
        ]);
    }
}
