<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\currency;

class currencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = new currency();
        return $currencies->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency = new currency();
        $name = $request -> input('name');
        $rate = $request -> input('rate');
        $ISO = $request -> input('ISO');
        $Symbol = $request -> input('Symbol');
        $slug = $request -> input('slug');
        $currency -> name = $name;
        $currency -> rate = $rate;
        $currency -> ISO = $ISO;
        $currency -> Symbol = $Symbol;
        $currency -> slug = $slug;
        $currency -> save();
        return response()->json([
            "Currency was created"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currency = currency::where('id', $id)->get();

        return response()->json([
            "currency" => $currency
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = currency::where('id', $id);
        $inputsCurrency = $request -> except(['_method', 'token']);
        $currency -> update($inputsCurrency);
        return response()->json([
            "Currency has been updated"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = currency::where('id', $id);
        $currency -> delete();
        return response()->json([
            "currency has been deleted"
        ]);
    }
}
